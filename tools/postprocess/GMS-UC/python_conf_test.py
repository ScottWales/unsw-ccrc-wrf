# testing python configuration
import numpy as np
import scipy
from Scientific.IO.NetCDF import *
from optparse import OptionParser
import subprocess as sub
from glob import glob
import sys, time, string, csv, os
import datetime as dt
from netCDF4 import Dataset as NetCDFFile
from pyclimate.JDTimeHandler import *
from pyclimate.ncstruct import *
import numpy.oldnumeric as Numeric
import matplotlib as plt
