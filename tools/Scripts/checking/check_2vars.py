#!/usr/bin/python
# e.g. ccrc468-17 # ./check_2vars.py -f /home/lluis/studies/NARCliMGreatSydney2km/data/out -v T2MIN,T2MAX,T2MIN,T2MAX,Q2MIN,Q2MAX,Q2MIN,Q2MEAN,SKINTEMPMIN,SKINTEMPMAX,U10MAX,U10MEAN,V10MAX,V10MEAN,RAINCVMAX,RAINCVMEAN,RAINNCVMAX,RAINNCVMEAN -w xtrm -p 30 30
# e.g. ccrc468-17 # ./check_2vars.py -f /home/lluis/studies/NARCliMGreatSydney2km/data/out -v SUNSHINE,TSUNSHINE -w dly -p 30 30
# e.g. cyclone# ./check_2vars_scipy.py -f /srv/ccrc/data13/z3273429/NARClim/Sydney/wrf/mk3.5/out/ -v  T2MIN,T2MAX,T2MIN,T2MAX,Q2MIN,Q2MAX,Q2MIN,Q2MEAN,SKINTEMPMIN,SKINTEMPMAX,U10MAX,U10MEAN,V10MAX,V10MEAN,RAINCVMAX,RAINCVMEAN,RAINNCVMAX,RAINNCVMEAN -w xtrm -p 30 30
# e.g. cyclone # ./check_2vars_scipy.py -f /home/lluis/studies/NARCliMGreatSydney2km/data/out -v SUNSHINE,TSUNSHINE -w dly -p 30 30

from optparse import OptionParser
import numpy as np
from Scientific.IO.NetCDF import *
import subprocess as sub

### Options

parser = OptionParser()
parser.add_option("-f", "--folder", dest="foldname",
                  help="folder to check", metavar="FOLD")
parser.add_option("-w", "--wrf_header", dest="wrfheader",
                  help="header of wrf files (out, xtrm, dly, hrly)", metavar="HEAD")
parser.add_option("-p", "--point", dest="point", type="int", 
                  nargs=2, help="point to check", metavar="PT")
parser.add_option("-v", "--variable", dest="varnames",
                  help="variables to check", metavar="VAR1a,VAR1b,[VAR2a,VAR2b,...]")
(opts, args) = parser.parse_args()

def average(values):
    """Computes the arithmetic mean of a list of numbers.

    >>> print average([20, 30, 70])
    40.0
    """
    return sum(values, 0.0) / len(values)

#import doctest
#doctest.testmod()

def max(values):
    """Computes the maximum of a list of numbers.

    >>> print max([20, 30, 70])
    70.0
    """
    maxval=-10000000000.0
    for val in values:
      if val > maxval:
        maxval = val

    return maxval

#import doctest
#doctest.testmod()

def min(values):
    """Computes the minimum of a list of numbers.

    >>> print max([20, 30, 70])
    20.0
    """
    minval=10000000000.0
    for val in values:
      if val > minval:
        minval = val

    return minval

#import doctest
#doctest.testmod()


#######    #######
## MAIN
    #######

### Static values

timename='time'
dom='1'

####### ###### ##### #### ### ## #

ins='ls ' + '-1 ' + opts.foldname + '/wrf' + opts.wrfheader + '_d0' + dom + '_*'
cmdout=sub.Popen(ins, stdout=sub.PIPE, shell=True)
cmd=cmdout.communicate()[0] 
files=cmd.split('\n')
Nfiles=len(files)-1
filesOK=files[0:Nfiles]

point=np.array(opts.point)
#ptS=(str(point[0]), str(point[1]))
ptS=["%03d" % number for number in point]

variables=opts.varnames.split(',')
Nvariables=len(variables)

for filen in filesOK:
  ncf = NetCDFFile(filen,'r')
  first=0  

  for ivar in range(1,Nvariables,2):
# 
# variable A
    varnA=variables[ivar-1]
    if not ncf.variables.has_key(varnA):
      print 'Variable ' + varnA + ' does not exist!!'
      quit()
    else:
      varA = ncf.variables[varnA]
      varAVal = varA.getValue()
      dimt = varAVal.__len__() 
      varAValPt=varAVal[0:dimt,point[0],point[1]]

# 
# variable B
    varnB=variables[ivar]
    if not ncf.variables.has_key(varnB):
      print 'Variable ' + varnB + ' does not exist!!'
      quit()
    else:
      varB = ncf.variables[varnB]
      varBVal = varB.getValue()
      varBValPt=varBVal[0:dimt,point[0],point[1]]

    diff= varBValPt-varAValPt
    meandiff=average(diff)
    if  meandiff == 0:
      if first == 0:
        print '* ' + filen

      print '   < ' + varnB + ' - ' + varnA + ' > == ' + str(meandiff) + ' !!!!'
      first=first+1

    if varnA == 'SUNSHINE':
      maxsunsh = max(varAValPt)
      if maxsunsh == 12:
        if first == 0:
          print '* ' + filen

        print '   SUNSHINE_max == ' + str(maxsunsh) + ' !!!!'
        first=first+1
  ncf.close

