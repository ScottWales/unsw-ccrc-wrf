## e.g. # adding_TimeBounds.py -f [file] -r 19491201000000 -q MO -s 1 

import numpy as np
from netCDF4 import Dataset as NetCDFFile
import os
from optparse import OptionParser
import re

def searchInlist(listname, nameFind):
  for x in listname:
    if x == nameFind:
      return True
  return False

def month_bounds(RefDate, hours):
    """ Gives de bounds of a month ([YYYY]-[MM]-01, [YYYY]-[MM+1]-01) from a given number of hours according to a referebce date [RefDate]

    >>> print month_bounds('19500101000000', 229032)
    [228648 229344]
    """
    import datetime as dt
    import numpy as np

    refdate = dt.datetime(int(RefDate[0:4]), int(RefDate[4:6]), int(RefDate[6:8]), int(RefDate[8:10]), int(RefDate[10:12]), int(RefDate[12:14]))
    newdate = refdate + dt.timedelta(seconds=hours*3600)
    newdateval = np.array([int(newdate.strftime("%Y")), int(newdate.strftime("%m")), int(newdate.strftime("%d")), int(newdate.strftime("%H")), 
      int(newdate.strftime("%M")), int(newdate.strftime("%S"))])
    hoursmonthref = hours - (newdateval[2]-1)*24 - (newdateval[3]) - newdateval[4]/60 - newdateval[5]/3600
    mon1dateval=newdateval
    mon1dateval[1]=mon1dateval[1]+1
    if mon1dateval[1] > 12:
      mon1dateval[1] = 1
      mon1dateval[0] = mon1dateval[0] + 1

    mon1dateval[2]=1
    mon1dateval[3]=0
    mon1dateval[4]=0
    mon1dateval[5]=0
    refmon1date = dt.datetime(mon1dateval[0], mon1dateval[1], mon1dateval[2], mon1dateval[3], mon1dateval[4], mon1dateval[5])
    diffmon1 = refmon1date - refdate
    hoursmon1ref=diffmon1.days*24 + diffmon1.seconds/3600
    return np.array([int(hoursmonthref), int(hoursmon1ref)])

def month_mid(RefDate, hours):
    """ Gives de mid date of a month ([YYYY]-[MM]-01, [YYYY]-[MM+1]-01)/2 from a given number of hours according to a referebce date [RefDate]

    >>> print month_mid('19500101000000', 229032)
    228996
    """
    import datetime as dt
    import numpy as np

    refdate = dt.datetime(int(RefDate[0:4]), int(RefDate[4:6]), int(RefDate[6:8]), int(RefDate[8:10]), int(RefDate[10:12]), int(RefDate[12:14]))
    newdate = refdate + dt.timedelta(seconds=hours*3600)
    newdateval = np.array([int(newdate.strftime("%Y")), int(newdate.strftime("%m")), int(newdate.strftime("%d")), int(newdate.strftime("%H")), 
      int(newdate.strftime("%M")), int(newdate.strftime("%S"))])
    hoursmonthref = hours - (newdateval[2]-1)*24 - (newdateval[3]) - newdateval[4]/60 - newdateval[5]/3600
    mon1dateval=newdateval
    mon1dateval[1]=mon1dateval[1]+1
    if mon1dateval[1] > 12:
      mon1dateval[1] = 1
      mon1dateval[0] = mon1dateval[0] + 1

    mon1dateval[2]=1
    mon1dateval[3]=0
    mon1dateval[4]=0
    mon1dateval[5]=0
    refmon1date = dt.datetime(mon1dateval[0], mon1dateval[1], mon1dateval[2], mon1dateval[3], mon1dateval[4], mon1dateval[5])
    diffmon1 = refmon1date - refdate
    hoursmon1ref=diffmon1.days*24 + diffmon1.seconds/3600
    return int(hoursmonthref) + int((int(hoursmon1ref) - int(hoursmonthref))/2)

#print string_operation

parser = OptionParser()
parser.add_option("-c", "--cellTimeMethod", dest="cTmethod", 
                  help="cell time method to write as attribute in the variable in cell_method", metavar="LABEL")
parser.add_option("-f", "--netCDF_file", dest="ncfile", 
                  help="file to use", metavar="FILE")
parser.add_option("-q", "--frequencyName", type='choice', dest="freqname", choices=['01H', '03H', '06H', '12H', '24H', 'MO', 'DA'], 
                  help="frequencies: 01H, 03H, 06H, 12H, 24H, MO, DA", metavar="LABEL")
parser.add_option("-r", "--ReferenceDate", dest="refdate", 
                  help="reference date ([YYYY][MM][DD][HH][MI][SS]", metavar="VALUES")
parser.add_option("-s", "--stat", dest="varstat",
                  help="is variable a statistic value?", metavar="VARFLAG")
parser.add_option("-v", "--varname", dest="varn",
                  help="variable name", metavar="VARNAME")

(opts, args) = parser.parse_args()
#######    #######
# MAIN

errormsg='ERROR -- error -- ERROR -- error'

####### ###### ##### #### ### ## #

ncfile = opts.ncfile
varstat = opts.varstat
freqname = opts.freqname
refdate = opts.refdate

if not os.path.isfile(ncfile):
  print errormsg
  print '  File ' + ncfile + ' does not exist !!'
  print errormsg
  quit()    

inc = NetCDFFile(ncfile,'a')

varn=opts.varn
if inc.dimensions.has_key('plev'):
  # removing pressure level from variable name
  varn = re.sub("\d+", "", varn) 

timename='time'
if not inc.dimensions.has_key(timename):
  print 'ERROR -- error -- ERROR -- error '
  print ncfile + ' does not have the variable ' + timename 
  print 'ERROR -- error -- ERROR -- error '
  quit()

timeVar=inc.variables[timename]
timevals = timeVar[:]

Nvalues=len(timevals)
timebounds=np.reshape(np.arange(Nvalues*2),(Nvalues,2))

if freqname == 'MO':
  monmids = timevals
  for t in range(Nvalues):
    monmids[t-1] =  month_mid(refdate, timevals[t-1])

  timeVar[:] = monmids

for t in range(Nvalues):
  if freqname == 'MO':
    monbounds = month_bounds(refdate, timevals[t-1])
    timebounds[t-1][0] = monbounds[0]
    timebounds[t-1][1] = monbounds[1]
  else:
    dtime=(timevals[1]-timevals[0])/2
    if varstat == 1:
      timebounds[t-1][0] = timevals[t-1]-dtime
      timebounds[t-1][1] = timevals[t-1]+dtime
    else:
      timebounds[t-1][0] = timevals[t-1]-dtime*2
      timebounds[t-1][1] = timevals[t-1]

if not inc.dimensions.has_key('bnds'):
  print 'Creating bnds dimension '
  inc.createDimension('bnds', 2) 

if inc.dimensions.has_key('gsize') and not inc.dimensions.has_key('bnds'):
  inc.renameDimension('gsize', 'bnds')

Dtime_bounds=('time','bnds')

if not inc.variables.has_key('time_bnds'):
  print 'Creating time_bnds variable '  
  timeBnds = inc.createVariable('time_bnds', 'd', Dtime_bounds) 
else:
  timeBnds = inc.variables['time_bnds']

timeBnds[:] = timebounds

# Attributes
#
varcell=inc.variables[varn]
attvarcell = varcell.ncattrs()

if searchInlist(attvarcell, 'cell_method'):
  attvalvarcell = varcell.getncattr('cell_method')
  if not 'time: minimum within monthly values ' == attvalvarcell:
    print '  cellmethod.bash: previous cell_method: ' + attvalvarcell
    attr = varcell.delncattr('cell_method')
    attr = varcell.setncattr('cell_method', opts.cTmethod + '  ' + attvalvarcell)
else:
  attr = varcell.setncattr('cell_method', opts.cTmethod)

if not searchInlist(attvarcell, 'cell_measures'):
  attr = varcell.setncattr('cell_measures', 'area: areacella')

attr = inc.setncattr('creation_date', '2012/06/15 01:24:50 UTC')

inc.sync()
inc.close()
