# README #

Welcome to CCRC-WRF repo. This is an improved version of Weather Research and Forecasting Model developed at Climate Change Research Centre, University of New South Wales, Australia. The improvements include monthly Greenhouse Gas Concentrations (GHGs), and additional output diagnostics.

The wiki section has more information on the additional output diagnostics, and on setting up the model for long climate simulations. 

No user support is provided beyond the information contained in the documents within this repository.

WRFV3.6.0.5.tar => This is the archive of WRF3.6.0.5 code with registry
                   changes for hourly output of surface energy balance
                   variables. This code was used for ERA-Interim runs. 
                   The code is originally developed from commit #e65aae
                   WARNING: These registry changes include neither
                   accumulated surface evaporation (SFCEVP), nor potential
                   evaporation (POTEVP), nor total soil moisture (SMSTOT). 

### Who do I talk to? ###

* Claire Carouge: c.carouge@unsw.edu.au
* Jason P. Evans: jason.evans@unsw.edu.au
* Roman Olson:    roman.olson@unsw.edu.au