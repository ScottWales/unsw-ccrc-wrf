##!/usr/bin/python
# e.g. # python var_time_evol.py -f /home/lluis/studies/NARCliMGreatSydney2km/data/d01/postPROJ/1990-1999/CCRC_NARCliM_Sydney_DAM_1997-1997_pslmax.nc -v pslmax -p 33 33 30 129 -t time
# e.g. # python var_time_evol.py -f /home/lluis/studies/NARCliMGreatSydney2km/data/d01/postPROJ/1990-1999/CCRC_NARCliM_Sydney_DAM_1997-1997_pslmax.nc -v pslmax -l 150.477 -33.312 146.204 -23.1519 -N lon -n lat -t time

from optparse import OptionParser
import numpy as np
from Scientific.IO.NetCDF import *
from pylab import *
import subprocess as sub
import datetime as dt
import matplotlib as plt

def lonlat_point(ncfile, lonname, latname, lonv, latv):
    """ Function to provide the grid point of a lon and lat matrices in a file 
        of a lon, lat position
    >>> print lonlat_point('/home/lluis/studies/NARCliMGreatSydney2km/data/d01/postPROJ/1990-1999/CCRC_NARCliM_Sydney_DAM_1997-1997_pslmax.nc',
        'lon', 'lat', 146.204, -23.1519)
    [104, 70]
    """
    import numpy as np
    from netCDF4 import Dataset as NetCDFFile

    ncfl = NetCDFFile(ncfile, 'r')

    if not ncfl.variables.has_key(lonname):
        print 'File does not have longitudes as ' + lonname + ' !!!!'
        quit()

    if not ncfl.variables.has_key(latname):
        print 'File does not have latitudes as ' + latname + ' !!!!'
        quit()

    lon = ncfl.variables[lonname]
    longitudes = lon[:]
    lat = ncfl.variables[latname]
    latitudes = lat[:]

    ncfl.close()

    minlon=np.amin(longitudes)
    maxlon=np.amax(longitudes)
    minlat=np.amin(latitudes)
    maxlat=np.amax(latitudes)

    if minlon <= lonv and maxlon >= lonv and minlat <= latv and maxlat >= latv:
        diff = np.sqrt((longitudes-lonv)*(longitudes-lonv) + (latitudes-latv)*(latitudes-latv))
        minpos = argmin(diff)
        xminpos = minpos/diff.shape[1]
        yminpos = minpos - xminpos*diff.shape[1]
    else:
        print 'Point ',lonv,',', latv, 'lays outside the matrix! (',minlon,',',minlat,') x (', \
          maxlon,',',maxlat,')'
        xminpos = yminpos = 1
        quit()

    return [xminpos, yminpos]

### Options

parser = OptionParser()
parser.add_option("-f", "--file", dest="ncfile",
                  help="file to check", metavar="FILE")
parser.add_option("-l", "--lonlatpoint", dest="lonlatpoint", type="float", 
                  nargs=4, help="lont lat pairs of points to check", metavar="PT")
parser.add_option("-N", "--LongitudeName", dest="lonn",
                  help="name of the longitude variable in the file", metavar="NAME")
parser.add_option("-n", "--LatitudeName", dest="latn",
                  help="name of the latitude variable in the file", metavar="NAME")
parser.add_option("-p", "--point", dest="point", type="int", 
                  nargs=4, help="pair of points to check", metavar="PT")
parser.add_option("-t", "--timename", dest="timename",
                  help="name of time", metavar="TIME")
parser.add_option("-v", "--variable", dest="varname",
                  help="variable to check", metavar="VAR")
(opts, args) = parser.parse_args()

#######    #######
## MAIN
    #######
errormsg='ERROR -- error -- ERROR -- error'

if not os.path.isfile(opts.ncfile):
  print errormsg
  print '  File ' + opts.ncfile + ' does not exist !!'
  print errormsg
  quit()

if not opts.lonlatpoint is None:
  Llpoint = np.array(opts.lonlatpoint)
  point = np.zeros(4)
  pt = lonlat_point(opts.ncfile, opts.lonn, opts.latn, Llpoint[0], Llpoint[1])
  point[0]=pt[0]
  point[1]=pt[1]
  pt = lonlat_point(opts.ncfile, opts.lonn, opts.latn, Llpoint[2], Llpoint[3])
  point[2]=pt[0]
  point[3]=pt[1]
else:
  point=np.array(opts.point)

#ptS=(str(point[0]), str(point[1]))
ptS=["%03d" % number for number in point]

ncf = NetCDFFile(opts.ncfile, 'r')

if not ncf.variables.has_key(opts.timename):
  print 'File does not have time !!!!'
  quit()

timeVar = ncf.variables[opts.timename]
timeVarVal = timeVar.getValue()
timeUnits=getattr(timeVar, 'units')

var = ncf.variables[opts.varname]
varVal = var.getValue()
dimt = varVal.__len__() 
varUnits=getattr(var, 'units')
varshape = len(varVal.shape)

if varshape == 3:
  varValPt1=varVal[0:dimt,point[0],point[1]]
  varValPt2=varVal[0:dimt,point[2],point[3]]
else:
  varValPt1=varVal[0:dimt,0,point[0],point[1]]
  varValPt2=varVal[0:dimt,0,point[2],point[3]]

ncf.close
# Generatig dates
#
refdate0=dt.datetime(0001, 1, 1, 0, 0, 0)
refdate=dt.datetime(1949, 12, 1, 0, 0, 0)

timeVals=timeVarVal
times = []
dimt = varVal.__len__() 
for it in range(dimt):
  timeV = refdate + dt.timedelta(seconds=timeVarVal[it]*3600)
  difftime0 = timeV - refdate0
  timeVals[it] = float(difftime0.days) + float(difftime0.seconds)/(3600*24)

#
# Plotting

graphname='checking_pt_date_' + ptS[0] + '-' + ptS[1] + '_' + opts.varname
plt.pyplot.hold(True)

fig = plt.pyplot.figure()
ax = fig.add_subplot(111)
if not opts.lonlatpoint is None:
  ax.plot_date(timeVals, varValPt1, 'red', label='@ ' + str(Llpoint[0]) + ', ' + str(Llpoint[1]),
    xdate=True)
  ax.plot_date(timeVals, varValPt2, 'blue', label='@ ' +str(Llpoint[2]) + ', ' + str(Llpoint[3]),
    xdate=True)
else:
  ax.plot_date(timeVals, varValPt1, 'red', label='@ ' + ptS[0] + ', ' + ptS[1], xdate=True)
  ax.plot_date(timeVals, varValPt2, 'blue', label='@ ' + ptS[2] + ', ' + ptS[3], xdate=True)

title(opts.varname + ' at two grid points')
xlabel(opts.timename + ' (' + timeUnits + ')')
ylabel(opts.varname + ' (' + varUnits + ')')

xdatesfmt = plt.dates.DateFormatter('%m')
ax.xaxis.set_major_formatter(xdatesfmt)
#plt.pyplot.xticks(rotation=45)

plt.pyplot.legend()
plt.pyplot.savefig(graphname)

sub.call('display ' + graphname + '.png &', shell=True)
