#!/bin/bash 

for file in $1/*nc; do
  varn=`python WRF4G/util/python/nc_var.py -f ${file} -o infvars | grep -v lon | grep -v lat | grep -v height | grep -v time \
    | grep -v time_bnds | grep -v  Lambert_Conformal | grep -v '#'`
  echo ${file}
  ncdump -h ${file} | grep ${varn}':'

done # end of files
