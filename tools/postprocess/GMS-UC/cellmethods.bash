#!/bin/bash 
# e.g. # ./cellmethods.bash /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/abc/CCRC_NARCliM_GreatSydney_All_2000-2009_mrso.nc mrso out All /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/namelist.output 1 /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G
if test $1 = '-h'; then
  echo "*******************************"
  echo "*** Shell to introduce cell ***"
  echo "***  methods on the output  ***"
  echo "***      project files      ***"
  echo "*******************************"
  echo "cellmethods.bash [netcdf] [varname] [wrf kind] [freq name] [nameslist output] [domain] [WRF4Ghome]"
  echo "  [netcdf]: netcdf file"
  echo "  [varname]: name of the variable"
  echo "  [wrf kind]: kind of wrfoutput ('out', 'xtrm', 'dly', 'hrly', ...)"
  echo "  [freq name]: name of the frequency in the output file"
  echo "  [namelist output]: namelist.output from the simulations"
  echo "  [domain]: number of the domain"
  echo "  [WRF4Ghome]: absolute path to the 'WRF4G' folder"
else
####### Functions
# get_Ntimes_namelist: Function to get Ntimes from a namelist.output of a given kind of wrf file and a given domain
# check_time_file: File time-steps it is assumed that file is in CF-compilant format ('time' is the name of variable time)
# check_stats_frqn: Function to check the name of a frquency output file ('@' will be transformed to a ' ' )
# function check_stats_frqvarn: Function to check the name of a frquency output file ('@' will be transformed to a ' ' ) using the frequency and
#   the variable name
# check_stats_varn: Function to check if variable name has a statistic section
# CF_CORDEX_cellmethods: Function to provide to the netCDF files with the variable cellmethod attributes of the CORDEX specifications
# CF_CORDEX_cellspace: Cell space method of the variable
# add_time_bnds_cellmethod: Adding 'time_bnds' variable in the netCDF file
#   File time-steps it is assumed that file is in CF-compilant format ('time' is the name of variable time)
#   Adding also date of creation of the file

get_Ntimes_namelist(){
# Function to get Ntimes from a namelist.output of a given kind of wrf file and a given domain
#
### Variables
# namlst: 'namelist.output' to use to find the 
# wrfdom: wrf domain
# fk: wrf kind of file 'out', 'xtrm', 'dly', 'hrly',... (must much with value in namelist.output as 'AUXHIST[NN]_OUTNAME')

  namlst=$1
  wrfdom=$2
  fk=$3

  auxhist=`cat ${namlst} | grep ${fk} | awk '{print $1}' | tr '_' ' ' | awk '{print $1}'`
  histfrq=`cat ${namlst} | grep ${auxhist}_INTERVAL | grep -v _[YDHMS] ` # Assuming that [AUXHIST]_INTERVAL_[Y/D/H/M/S] is not used
# interval
#
  nvalues=`echo $histfrq | awk '{print $3}' | tr '*' ' ' | awk '{print $1}'`
  if test ${nvalues} -ge $wrfdom; then
    histfrq=`echo $histfrq | awk '{print $3}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
  else
    histfrq=`echo $histfrq | awk '{print $4}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
  fi
# frames x file
# 
  if test ${fk} = 'out'; then
    nvalues=`cat ${namlst} | grep FRAMES_PER_OUTFILE | awk '{print $3}' | tr '*' ' ' | awk '{print $1}'`
    if test  ${nvalues} -ge $wrfdom; then
      framesfile=`cat ${namlst} | grep FRAMES_PER_OUTFILE | awk '{print $3}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
    else
      framesfile=`cat ${namlst} | grep FRAMES_PER_OUTFILE | awk '{print $4}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
    fi
  else
    nvalues=`cat ${namlst} | grep FRAMES_PER_${auxhist} | awk '{print $3}' | tr '*' ' ' | awk '{print $1}'`
    if test  ${nvalues} -ge $wrfdom; then
      framesfile=`cat ${namlst} | grep FRAMES_PER_${auxhist} | awk '{print $3}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
    else
      framesfile=`cat ${namlst} | grep FRAMES_PER_${auxhist} | awk '{print $4}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
    fi
  fi

# Internal time_step (seconds)
#
  timeStepSec=`cat ${namlst} | grep ' TIME_STEP ' | awk '{print $3}' | tr ',' ' '`
  echo $histfrq" "$framesfile" "$timeStepSec
}

function check_time_file() {
# File time-steps it is assumed that file is in CF-compilant format ('time' is the name of variable time)
# 
### Variables
# varN: name of the variable
# netcdf: netcdf file
# wrfk: kind of wrf file
# frq: name of the desired frequency
# nlstout: 'namelist.output' as reference of the simulations
# domN: number of the domain
# WRF4h: place with the WRF4G structure ( '$WRF4h/util/python' )
# 
  varN=$1
  netcdf=$2
  wrfk=$3
  frqn=$4
  nlstout=$5
  domN=$6
  WRF4h=$7

  refdateYmdHMS=${RefDate:0:4}${RefDate:5:2}${RefDate:8:2}${RefDate:11:2}${RefDate:14:2}${RefDate:17:2}

##  filetimeinf=`$WRF4h/util/python/nc_time_tools.py -f ${netcdf} -t time -r ${refdateYmdHMS} -F 1 -o timeINF`
##  Ntimes=`$WRF4h/util/Fortran/nc_file_inf ${netcdf} | grep -a --text DIMinf | grep time | awk '{print $4}'`
##  filetimes=`$WRF4h/util/Fortran/nc_var_out ${netcdf} time 1 1 1 1 1 1 -1 1 1 1 1 1 | grep -v '#'`
##  file1sttime=`echo ${filetimes} | awk '{print $1}' | tr 'D' 'E'`
##  filelasttime=`echo ${filetimes} | tr ' ' '\n' | tail -n 1 | tr 'D' 'E'`
##  difftime=`echo ${filelasttime}" "${file1sttime}" "${Ntimes}" 1" | awk '{print ($1 - $2)/( $3 - $4)}'`

  simTimeInf=`get_Ntimes_namelist ${nlstout} ${domN} ${wrfk}`

  timefileinf=`echo ${simTimeInf} | awk '{print $1*60}'`
  timefileinf1='point@values@'${timefileinf}'@second'

#
# extreme values use 'time_step' to compute the results clWRF modifications
#
  #timefileinf=`echo ${simTimeInf} | awk '{print $3}'`
  timefileinf=`python ${WRF4Ghome}/util/python/nc_var.py -f ${FULLnc} -o infsinggattrs -S DT | awk '{print $3}'`
  timefileinf0='point@values@'${timefileinf}'@second'

  xtrmstat=`check_stats_varn ${varN}`
  xtrmperiod=`echo ${simTimeInf} | awk '{print $1/60}'`
  case ${xtrmstat} in
    'min')      timefileinf='minimum@'${xtrmperiod}'@hour@value@from@'${timefileinf0}               ;;
    'max')      timefileinf='maximum@'${xtrmperiod}'@hour@value@from@'${timefileinf0}               ;;
    'mean')     timefileinf='mean@'${xtrmperiod}'@hour@value@from@'${timefileinf0}                  ;;
    'std')      timefileinf='standard@deviation@'${xtrmperiod}'@hour@value@from@'${timefileinf0}    ;;
    *)          timefileinf=${timefileinf1} 
  esac

# Specific cases
#
  case ${varN} in
    'pr5max')        timefileinf='maximum@5@minute@values@from@'${timefileinf0}     ;;
    'pr10max')       timefileinf='maximum@10@minute@values@from@'${timefileinf0}    ;;
    'pr20max')       timefileinf='maximum@20@minute@values@from@'${timefileinf0}    ;;
    'pr30max')       timefileinf='maximum@30@minute@values@from@'${timefileinf0}    ;;
    'pr1Hmax')       timefileinf='maximum@1@hour@values@from@'${timefileinf0}       ;;
    'wss5max')       timefileinf='maximum@5@minute@values@from@'${timefileinf0}     ;;
    'wss10max')      timefileinf='maximum@10@minute@values@from@'${timefileinf0}    ;;
    'wss20max')      timefileinf='maximum@20@minute@values@from@'${timefileinf0}    ;;
    'wss30max')      timefileinf='maximum@30@minute@values@from@'${timefileinf0}    ;;
    'wss1Hmax')      timefileinf='maximum@1@hour@values@from@'${timefileinf0}       ;;
    'pr5maxtstep')   timefileinf='maximum@5@minute@time-window@moving@averaged@values@from@'${timefileinf0}     ;;
    'pr10maxtstep')  timefileinf='maximum@10@minute@time-window@moving@averaged@values@from@'${timefileinf0}    ;;
    'pr20maxtstep')  timefileinf='maximum@20@minute@time-window@moving@averaged@values@from@'${timefileinf0}    ;;
    'pr30maxtstep')  timefileinf='maximum@30@minute@time-window@moving@averaged@values@from@'${timefileinf0}    ;;
    'pr1Hmaxtstep')  timefileinf='maximum@1@hour@time-window@moving@averaged@values@from@'${timefileinf0}       ;;
    'wss5maxtstep')  timefileinf='maximum@5@minute@time-window@moving@averaged@values@from@'${timefileinf0}     ;;
    'wss10maxtstep') timefileinf='maximum@10@minute@time-window@moving@averaged@values@from@'${timefileinf0}    ;;
    'wss20maxtstep') timefileinf='maximum@20@minute@time-window@moving@averaged@values@from@'${timefileinf0}    ;;
    'wss30maxtstep') timefileinf='maximum@30@minute@time-window@moving@averaged@values@from@'${timefileinf0}    ;;
    'wss1Hmaxtstep') timefileinf='maximum@1@hour@time-window@moving@averaged@values@from@'${timefileinf0}       ;;
  esac

  echo ${timefileinf}
}

function check_stats_frqvarn() {
# Function to check the name of a frquency output file ('@' will be transformed to a ' ' ) using the frequency and the variable name
  freqn=$1
  varn=$2

  TimeWindow=${freqn:0:2}
  statfrq=${freqn:2:1}

  if test ${statfrq} = 'H'; then
    Tunit='hour'
  else
    Tunit='month'
  fi

  case $TimeWindow in
    'DA')   timeval='daily@values'                ;;
    'MO')   timeval='monthly@values'              ;;
    'SE')   timeval='seasonal@values'             ;;
    'YE')   timeval='yearly@values'               ;;
    '24')   timeval='24@'${Tunit}'@point@value'   ;;
    '12')   timeval='12@'${Tunit}'@point@value'   ;;
    '06')   timeval='6@'${Tunit}'@point@value'    ;;
    '03')   timeval='3@'${Tunit}'@point@value'    ;;
    '01')   timeval='1@'${Tunit}'@point@value'    ;;
  esac

  statname=`check_stats_varn ${varn}`
  case $statname in
    'mean') statN='mean@within@'                  ;;
    'ac')   statN='accumulation@within@'          ;;
    'min')  statN='minimum@within@'               ;;
    'max')  statN='maximum@within@'               ;;
    *)      statN='NONE'                          ;;
  esac

  case ${statfrq} in
    'H')   statN='every@'                         ;;
    'm')   statN='every@'                         ;;
  esac
  echo ${timeval}" "${statN}
}

function check_stats_frqn() {
# Function to check the name of a frquency output file ('@' will be transformed to a ' ' )
  freqn=$1

  TimeWindow=${freqn:0:2}
  statfrq=${freqn:2:1}

  if test ${statfrq} = 'H'; then
    Tunit='hour'
  else
    Tunit='month'
  fi

  case $TimeWindow in
    'DA')   timeval='daily@values'                ;;
    'MO')   timeval='monthly@values'              ;;
    'SE')   timeval='seasonal@values'             ;;
    'YE')   timeval='yearly@values'               ;;
    '24')   timeval='24@'${Tunit}'@point@value'   ;;
    '12')   timeval='12@'${Tunit}'@point@value'   ;;
    '06')   timeval='6@'${Tunit}'@point@value'    ;;
    '03')   timeval='3@'${Tunit}'@point@value'    ;;
    '01')   timeval='1@'${Tunit}'@point@value'    ;;
  esac
  case $statfrq in
    'M')   statN='mean@within@'                   ;;
    'S')   statN='accumulation@within@'           ;;
    'N')   statN='minimum@within@'                ;;
    'X')   statN='maximum@within@'                ;;
    'H')   statN='every@'                         ;;
    'm')   statN='every@'                         ;;
    *)     statN='NONE'                           ;;
  esac
  echo ${timeval}" "${statN}
}

function check_stats_varn() {
# Function to check if variable name has a statistic section
  varN=$1
  LvarN=`expr length ${varN}`
  statsnames='min max mean ac mintstep maxtstep meantstep actstep'

  statsvar='0'
  for statsn in ${statsnames}; do
    Lstatsn=`expr length ${statsn}`
    Lvarsec=`expr $LvarN - $Lstatsn`
    varsec=${varN:${Lvarsec}:${Lstatsn}}
    if test ${Lvarsec} -gt 1; then
      if test ${varsec} = ${statsn}; then
#
# Removing 'tstep' surname
        if test $Lstatsn -gt 4; then
          Llvarsec=`expr length $varsec`
          LvarsecNOtstep=`expr $Llvarsec - 5`
          varsec=${varsec:0:${LvarsecNOtstep}}
        fi
        statsvar=${varsec}
        break
      fi
    fi
  done # end of statsnames

# Specifics
##
  varspec='pracc@ac'

  varspecs=`echo ${varspec} ':' ' '`
  for ivar in ${varspecs}; do
    varspecn=`echo ${ivar} | tr '@' ' ' | awk '{print $1}'`
    if test ${varspecn} = ${varN}; then
      statsvar=`echo ${ivar} | tr '@' ' ' | awk '{print $2}'`
      break
    fi
  done 

  echo ${statsvar}
}

function is_stats_varn() {
# Function to check if variable name has a statistic section
  varN=$1
  LvarN=`expr length ${varN}`
  statsnames='min max mean ac mintstep maxtstep meantstep actstep'

  stats=1
  statsvar='0'
  for statsn in ${statsnames}; do
    Lstatsn=`expr length ${statsn}`
    Lvarsec=`expr $LvarN - $Lstatsn`
    varsec=${varN:${Lvarsec}:${Lstatsn}}
    if test ${Lvarsec} -gt 1; then
      if test ${varsec} = ${statsn}; then
        stats=0
        return ${stats}
      else
        stats=1
      fi
    fi
  done # end of statsnames

# particular cases
##
  specificvars='pracc:potevp:prls:prc:mrros:mross:evspsbl'
  specvars=`echo ${specificvars} | tr ':' ' '`
  for svar in ${specvars}; do
    if test ${varN} = ${svar}; then
      stats=0
      return ${stats}
    fi
  done

# Checking deaccumulation in the 'wrfncxnj.table' file
##
  WRFname=`WRFvarname ${varN}`
  deacc=`cat ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table | grep ${WRFname}' ' | grep deaccumulate | awk '{print $1}'`
  Ldeacc=`expr length ${deacc}'0'`
  if test ${Ldeacc} -gt 1; then
    stats=0
    return ${stats}
  fi

  WRFname=`WRFvarname ${varN}`
  deacc=`cat ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table | grep ${WRFname}' ' | grep deaccumulate_flux | awk '{print $1}'`
  Ldeacc=`expr length ${deacc}'0'`
  if test ${Ldeacc} -gt 1; then
    stats=0
    return ${stats}
  fi

  return ${stats}
}

function WRFvarname() {
# Function to give the name of the variable in the WRF file
#
### Variables
# varn: CF name of the variable
  varn=$1
  WRFnames=`cat ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table | grep ${varn} | awk '{print $1}'`

  for WRFn in ${WRFnames}; do
    WRFvname=`cat ${requestedfile} | grep ${WRFn} | head -n 1 | awk '{print $1}'`
    if test ${WRFvname} = '#'; then 
      WRFvname=`cat ${requestedfile} | grep ${WRFn} | head -n 1 | awk '{print $2}'`
    fi
    LWRFvname=`expr length ${WRFvname}'0'`
    if test ${LWRFvname} -gt 1; then
      return ${WRFvname}
    fi
  done
}

function CF_CORDEX_cellmethods() {
# Function to provide to the netCDF files with the variable cellmethod attributes of the CORDEX specifications
#
### Variables
# ncf: netCDF file
# varn: name of the variable
# wrfk: kind of wrf out 'out', 'xtrm', 'hrly', 'dly'
# freqn: name of the frequency 'All', '[DA/MO/SE/YE][M/S/X/N]', ...
# WRF4Ghome: Place with the structure of the WRF4G ($WRF4Ghome/util/python)

  ncf=$1
  varn=$2
  wrfk=$3
  freqn=$4
  nlstout=$5
  dom=$6
  WRf4Ghome=$7
  
# Is variable a statistic field?
# 
  varstat=`check_stats_varn ${varn}`

# Is frequency a statistic field?
# 
  celltime_method='time:@'
  if test ! ${freqn} = 'All'; then
    freqvlues=`check_stats_frqvarn ${freqn} ${varn}`
    timeName=`echo ${freqvlues} | awk '{print $1}'`
    statName=`echo ${freqvlues} | awk '{print $2}'`
    if test ! $statName = 'NONE'; then
      celltime_method=${celltime_method}${statName}${timeName}'@'
    else
      celltime_method=${celltime_method}
    fi
  else
    statName='NO'
  fi

# Checking if frequency output is the same as variable frequency
#
  simTInf=`get_Ntimes_namelist ${nlstout} ${dom} ${wrfk} | awk '{print $1}'`
  simTHInf=`expr ${simTInf} / 60`

  equalFrqSimT='0'
  if test $statName = 'every@'; then
    hourfrq=`expr ${freqn:0:2} + 0`
    if test ${hourfrq} -eq ${simTHInf} && test ! ${freqn:2:1} = 'm'; then # The extra if for ${freqn:2:1} = 'm' means that must be better done!
      celltime_method='time:@'
      equalFrqSimT='1'
    fi
  fi

# Original frequency time of the fields
#
  if test ${freqn} == 'All' || test ${equalFrqSimT} -eq 1; then
    filetime=`check_time_file ${varn} ${ncf} ${wrfk} ${freqn} ${nlstout} ${dom} ${WRF4Ghome}`
    celltime_method=${celltime_method}${filetime}
  fi

  echo ${celltime_method}
}

function CF_CORDEX_cellspace() {
# Cell space method of the variable
#
### Variables
# ncdf: netCDF file
# varN: variable name

  ncdf=$1
  varN=$2

  cellmetspace='area:@areacella'
  echo ${cellmetspace}
}


function add_time_bnds_cellmethod() {
# Adding 'time_bnds' variable in the netCDF file
# File time-steps it is assumed that file is in CF-compilant format ('time' is the name of variable time)
# Adding also date of creation of the file
# 
### Variables
# netcdf: netcdf file
# WRF4h: place with the WRF4G structure ( '$WRF4h/util/python' )
# varN: name of the variable
# frqn: name of the frequency of the output (if it is [MO][N/X/M/S], bounds will be ajusted to the month boundaries [MM]-01 <--> [MM+1]-01)
# RDate: Reference date for the dates ([YYYY][MM][DD][HH][MI][SS])
# celltime: cell_method attribute of the variable to be added
# cellmeasure: cell measure (space) of the variable 
# 

  ncf=$1
  WRF4h=$2
  varN=$3
  frqn=$4
  RDate=$5
  celltime=$6
  cellmeasure=$7
  period=$8

  dateval=`date -u "+%Y/%m/%d %H:%M:%S UTC"`

# Only min, max, mean will have centered 'time_bounds'
#
  varSTAT=0
  statN=`check_stats_varn ${varN}`
  if test ! ${statN} = '0'; then 
##    if test ! ${statN} = 'ac'; then varSTAT='1'; fi
    varSTAT='1'
  fi

  frqmon=${frqn:0:2}
  if test ${frqn:2:1} = 'm'; then frqmon='MO'; fi

  cellTime=`echo ${celltime} | tr '@' ' '`
  cellMeasure=`echo ${cellmeasure} | tr '@' ' '`

  cat << EOFpy > ${ROOTpost}/adding_TimeBounds.py
import numpy as np
from netCDF4 import Dataset as NetCDFFile
import os
import datetime as dt

def searchInlist(listname, nameFind):
  for x in listname:
    if x == nameFind:
      return True
  return False

def month_bounds(RefDate, hours):
    """ Gives de bounds of a month ([YYYY]-[MM]-01, [YYYY]-[MM+1]-01) from a given number of hours according to a referebce date [RefDate]

    >>> print month_bounds('19500101000000', 229032)
    [228648 229344]
    """
    import datetime as dt
    import numpy as np

    refdate = dt.datetime(int(RefDate[0:4]), int(RefDate[4:6]), int(RefDate[6:8]), int(RefDate[8:10]), int(RefDate[10:12]), int(RefDate[12:14]))
    newdate = refdate + dt.timedelta(seconds=hours*3600)
    newdateval = np.array([int(newdate.strftime("%Y")), int(newdate.strftime("%m")), int(newdate.strftime("%d")), int(newdate.strftime("%H")), 
      int(newdate.strftime("%M")), int(newdate.strftime("%S"))])
    hoursmonthref = hours - (newdateval[2]-1)*24 - (newdateval[3]) - newdateval[4]/60 - newdateval[5]/3600
    mon1dateval=newdateval
    mon1dateval[1]=mon1dateval[1]+1
    if mon1dateval[1] > 12:
      mon1dateval[1] = 1
      mon1dateval[0] = mon1dateval[0] + 1

    mon1dateval[2]=1
    mon1dateval[3]=0
    mon1dateval[4]=0
    mon1dateval[5]=0
    refmon1date = dt.datetime(mon1dateval[0], mon1dateval[1], mon1dateval[2], mon1dateval[3], mon1dateval[4], mon1dateval[5])
    diffmon1 = refmon1date - refdate
    hoursmon1ref=diffmon1.days*24 + diffmon1.seconds/3600
    return np.array([int(hoursmonthref), int(hoursmon1ref)])

def month_mid(RefDate, hours):
    """ Gives de mid date of a month ([YYYY]-[MM]-01, [YYYY]-[MM+1]-01)/2 from a given number of hours according to a referebce date [RefDate]

    >>> print month_mid('19500101000000', 229032)
    228996
    """
    import datetime as dt
    import numpy as np

    refdate = dt.datetime(int(RefDate[0:4]), int(RefDate[4:6]), int(RefDate[6:8]), int(RefDate[8:10]), int(RefDate[10:12]), int(RefDate[12:14]))
    newdate = refdate + dt.timedelta(seconds=hours*3600)
    newdateval = np.array([int(newdate.strftime("%Y")), int(newdate.strftime("%m")), int(newdate.strftime("%d")), int(newdate.strftime("%H")), 
      int(newdate.strftime("%M")), int(newdate.strftime("%S"))])
    hoursmonthref = hours - (newdateval[2]-1)*24 - (newdateval[3]) - newdateval[4]/60 - newdateval[5]/3600
    mon1dateval=newdateval
    mon1dateval[1]=mon1dateval[1]+1
    if mon1dateval[1] > 12:
      mon1dateval[1] = 1
      mon1dateval[0] = mon1dateval[0] + 1

    mon1dateval[2]=1
    mon1dateval[3]=0
    mon1dateval[4]=0
    mon1dateval[5]=0
    refmon1date = dt.datetime(mon1dateval[0], mon1dateval[1], mon1dateval[2], mon1dateval[3], mon1dateval[4], mon1dateval[5])
    diffmon1 = refmon1date - refdate
    hoursmon1ref=diffmon1.days*24 + diffmon1.seconds/3600
    return int(hoursmonthref) + int((int(hoursmon1ref) - int(hoursmonthref))/2)

def datetimeStr_datetime(StringDT):
  """ Function to transform a string date ([YYYY]-[MM]-[DD]_[HH]:[MI]:[SS] format ) to a date object
  >>> datetimeStr_datetime('1976-02-17_00:00:00')
  1976-02-17 00:00:00
  """
  import datetime as dt

  dateDT = StringDT.split('_')
  dateD = dateDT[0].split('-')
  timeT = dateDT[1].split(':')

  if int(dateD[0]) == 0:
    print warnmsg
    print '    datetimeStr_datetime: 0 reference year!! changing to 1'
    dateD[0] = 1 

  if len(timeT) == 3:
    newdatetime = dt.datetime(int(dateD[0]), int(dateD[1]), int(dateD[2]), int(timeT[0]), int(timeT[1]), int(timeT[2]))
  else:
    newdatetime = dt.datetime(int(dateD[0]), int(dateD[1]), int(dateD[2]), int(timeT[0]), int(timeT[1]), 0)

  return newdatetime

def dateStr_date(StringDate):
  """ Function to transform a string date ([YYYY]-[MM]-[DD] format) to a date object
  >>> dateStr_date('1976-02-17')
  1976-02-17
  """
  import datetime as dt

  dateD = StringDate.split('-')
  if int(dateD[0]) == 0:
    print warnmsg
    print '    dateStr_date: 0 reference year!! changing to 1'
    dateD[0] = 1
  newdate = dt.date(int(dateD[0]), int(dateD[1]), int(dateD[2]))
  return newdate

def timeref_datetime(refd, timeval, tu):
    """ Function to transform from a [timeval] in [tu] units from the time referece [tref] to datetime object
    refd: time of reference (as datetime object)
    timeval: time value (as [tu] from [tref])
    tu: time units
    >>> timeref = date(1949,12,1,0,0,0)
    >>> timeref_datetime(timeref, 229784.36, hours)
    1976-02-17 08:21:36
    """
    import datetime as dt
    import numpy as np

## Not in timedelta
#    if tu == 'years':
#        realdate = refdate + dt.timedelta(years=float(timeval))
#    elif tu == 'months':
#        realdate = refdate + dt.timedelta(months=float(timeval))
    if tu == 'weeks':
        realdate = refd + dt.timedelta(weeks=float(timeval))
    elif tu == 'days':
        realdate = refd + dt.timedelta(days=float(timeval))
    elif tu == 'hours':
        realdate = refd + dt.timedelta(hours=float(timeval))
    elif tu == 'minutes':
        realdate = refd + dt.timedelta(minutes=float(timeval))
    elif tu == 'seconds':
        realdate = refd + dt.timedelta(seconds=float(timeval))
    elif tu == 'milliseconds':
        realdate = refd + dt.timedelta(milliseconds=float(timeval))
    else:
          print errormsg
          print '    timeref_datetime: time units "' + tu + '" not ready!!!!'
          quit(-1)

    return realdate

def timeref_datetime_mat(refd, timeval, tu):
    """ Function to transform from a [timeval] in [tu] units from the time referece [tref] to matrix with: year, day, month, hour, minute, second
    refd: time of reference (as datetime object)
    timeval: time value (as [tu] from [tref])
    tu: time units
    >>> timeref = date(1949,12,1,0,0,0)
    >>> timeref_datetime(timeref, 229784.36, hours)
    [1976    2   17    8   36   21]
    """
    import datetime as dt
    import numpy as np


    realdates = np.zeros(6, dtype=int)
## Not in timedelta
#    if tu == 'years':
#        realdate = refdate + dt.timedelta(years=float(timeval))
#    elif tu == 'months':
#        realdate = refdate + dt.timedelta(months=float(timeval))
    if tu == 'weeks':
        realdate = refd + dt.timedelta(weeks=float(timeval))
    elif tu == 'days':
        realdate = refd + dt.timedelta(days=float(timeval))
    elif tu == 'hours':
        realdate = refd + dt.timedelta(hours=float(timeval))
    elif tu == 'minutes':
        realdate = refd + dt.timedelta(minutes=float(timeval))
    elif tunits == 'seconds':
        realdate = refd + dt.timedelta(seconds=float(timeval))
    elif tunits == 'milliseconds':
        realdate = refd + dt.timedelta(milliseconds=float(timeval))
    else:
          print errormsg
          print '    timeref_datetime: time units "' + tu + '" not ready!!!!'
          quit(-1)

    realdates[0] = int(realdate.year)
    realdates[1] = int(realdate.month)
    realdates[2] = int(realdate.day)
    realdates[3] = int(realdate.hour)
    realdates[4] = int(realdate.second)
    realdates[5] = int(realdate.minute)

    return realdates


class cls_time_information(object):
    """ Classs to provide information about variable time
    ncfu = netCDF unit name
    tname = name of the variable time in [ncfu]
    self.calendar: calendar of the variable time
    self.units: units of variable time
    self.Urefdate: reference date as it appears in the original 'units' section
    self.Srefdate: reference date as string [YYYY][MM][DD][HH][MI][SS]
    self.refdate: reference date
    self.attributes: attributes of time variable
    self.dimt: dimension of the time variable
    self.dt: distance between first two time-steps in self.tunits
    self.firstTu: first time value in self.units
    self.firstTt: first time value as datetime object
    self.firstTS: first time value as string [YYYY][MM][DD][HH][MI][SS]
    self.firstTm: first time value as matrix (from datetime; [Y], [M], [D], [H], [M], [S])
    self.lastTu: last time value in self.units
    self.firstTt: last time value as datetime object
    self.lastTS: last time value as string [YYYY][MM][DD][HH][MI][SS]
    self.lastTm: last time value as matrix (from datetime; [Y], [M], [D], [H], [M], [S])
    """

    import datetime as dt 

    def  __init__(self, ncfu, tname):
        fname='cls_time_information'
        wmsg=fname + ': WARNING -- warning -- WARNING -- warning'
        emsg=fname + ': ERROR -- error -- ERROR -- error'
        if ncfu is None:
            self.units = None
            self.calendar = None
            self.Urefdate = None
            self.Srefdate = None
            self.refdate = None
            self.attributes = None
            self.dimt = None
            self.dt = None
            self.firstTu = None
            self.firstTt = None
            self.firstTS = None
            self.firstTm = None
            self.lastTu = None
            self.lastTt = None
            self.lastTS = None
            self.lastTm = None
        else:
            times = ncfu.variables[tname]
            attvar = times.ncattrs() 
            self.attributes = attvar
            if not searchInlist(attvar, 'units'):
                print emsg
                print '    cls_time_information: time variable "', tname, '" does not have attribute: "units"'
                quit(-1)
            else:
                units = times.getncattr('units')

            if not searchInlist(attvar, 'calendar'):
                print wmsg
                print '    cls_time_information: time variable "', tname, '" does not have attribute: "calendar"'
                self.calendar = '-'
            else:
                self.calendar = times.getncattr('calendar')
  
            txtunits = units.split(' ')
            self.units = txtunits[0]
            Srefdate = txtunits[len(txtunits) - 1]
# Does reference date contain a time value [YYYY]-[MM]-[DD] [HH]:[MI]:[SS]
##
            timeval = Srefdate.find(':')

            if not timeval == -1:
#        print '  refdate with time!'
                self.refdate = datetimeStr_datetime(txtunits[len(txtunits) - 2] + '_' + Srefdate)
                self.Urefdate = txtunits[len(txtunits) - 2] + ' ' + Srefdate
            else:
                self.refdate = datetimeStr_datetime(Srefdate + '_00:00:00')
                self.Urefdate = Srefdate

            self.Srefdate = self.refdate.strftime("%Y%m%d%H%M%S")
            timev = times[:]

            self.dimt = times.shape[0]
            self.dt = timev[1]-timev[0]
            self.firstTu = timev[0]
            self.firstTt = timeref_datetime(self.refdate, self.firstTu, self.units)
            self.firstTS = self.firstTt.strftime("%Y%m%d%H%M%S")
            self.firstTm = timeref_datetime_mat(self.refdate, self.firstTu, self.units)
            self.lastTu = timev[self.dimt-1]
            self.lastTt = timeref_datetime(self.refdate, self.lastTu, self.units)
            self.lastTS = self.lastTt.strftime("%Y%m%d%H%M%S")
            self.lastTm = timeref_datetime_mat(self.refdate, self.lastTu, self.units)

#######    #######
# MAIN

ncfile='${ncf}'
varstat=${varSTAT}
freqname='${frqmon}'
refdate='${RDate}'
errormsg='ERROR -- error -- ERROR -- error'
period='${period}'

####### ###### ##### #### ### ## #
refdateS=refdate[0:4] + '-' + refdate[4:6] + '-' + refdate[6:8] + ' ' + refdate[8:10] + ':' + refdate[10:12] + ':' + refdate[12:14]

if not os.path.isfile(ncfile):
  print errormsg
  print '  File ' + ncfile + ' does not exist !!'
  print errormsg
  quit()    

inc = NetCDFFile(ncfile,'a')

if varstat == 1:

  print '  cellmethods.bash: variable "${varN}" is a temporal statistic. Adding variable "time_bnds"'

  timename='time'
  if not inc.dimensions.has_key(timename):
    print 'ERROR -- error -- ERROR -- error '
    print ncfile + ' does not have the variable ' + timename 
    print 'ERROR -- error -- ERROR -- error '
    quit()

  timeVar=inc.variables[timename]
  timevals = timeVar[:]

  timeinf = cls_time_information(inc, 'time')

  Nvalues=len(timevals)
  timebounds=np.reshape(np.arange(Nvalues*2),(Nvalues,2))

# Assuming that CDOs will put all the temporal values at the end of the period of the stats
#
#  filesecs=ncfile.split('_')
#  Nfsecs = len(filesecs)
#  if Nfsecs > 1:
#    period=filesecs[Nfsecs-2]
#    print 'filsecs: ',filesecs, ' period: ',period

#  years=period.split('-')
#  yearI=int(years[0])

#  date0=dt.datetime(yearI, 1, 1, 0, 0, 0) + dt.timedelta(hours=timeinf.dt/2)
#  firstdate = date0 - timeinf.refdate
#  firstdate = np.float64(firstdate.days * 24 + firstdate.seconds / 3600.)

  if freqname == 'MO':
    monmids = timevals
    for t in range(Nvalues):
      monmids[t-1] =  month_mid(refdate, timevals[t-1])

    timeVar[:] = monmids
  else:
    timeVar[:] = timevals - timeinf.dt/2.
    timevals = timeVar[:]

  for t in range(Nvalues):
    if freqname == 'MO':
      monbounds = month_bounds(refdate, timevals[t-1])
      timebounds[t-1][0] = monbounds[0]
      timebounds[t-1][1] = monbounds[1]
    else:
      dtime=(timevals[1]-timevals[0])/2.
      if varstat == 1:
        timebounds[t-1][0] = timevals[t-1]-dtime
        timebounds[t-1][1] = timevals[t-1]+dtime
      else:
        timebounds[t-1][0] = timevals[t-1]-dtime*2
        timebounds[t-1][1] = timevals[t-1]

  if not inc.dimensions.has_key('bnds'):
    print 'Creating bnds dimension '
    inc.createDimension('bnds', 2) 

  if inc.dimensions.has_key('gsize') and not inc.dimensions.has_key('bnds'):
    inc.renameDimension('gsize', 'bnds')

  Dtime_bounds=('time','bnds')

  if not inc.variables.has_key('time_bnds'):
    print 'Creating time_bnds variable '  
    timeBnds = inc.createVariable('time_bnds', 'd', Dtime_bounds) 
  else:
    timeBnds = inc.variables['time_bnds']

  timeBnds[:] = timebounds
  attrtimeBnds = timeBnds.ncattrs()
  if not searchInlist(attrtimeBnds, 'units'):
    attr = timeBnds.setncattr('units', 'hours since ' + refdateS)
  if not searchInlist(attrtimeBnds, 'calendar'):
    attr = timeBnds.setncattr('calendar', 'standard')

# time Attributes
#
  attrvtime=timeVar.ncattrs()

  if not searchInlist(attrvtime, 'bounds'):
    attr = timeVar.setncattr('bounds', 'time_bnds')


# Attributes
#
varcell=inc.variables['${varN}']
attvarcell = varcell.ncattrs()

if searchInlist(attvarcell, 'cell_method'):
  attvalvarcell = varcell.getncattr('cell_method')
  if not '${cellTime}' == attvalvarcell:
    print '  cellmethod.bash: previous cell_method: ' + attvalvarcell
    attr = varcell.delncattr('cell_method')
    attr = varcell.setncattr('cell_method', '${cellTime} ' + attvalvarcell)
else:
  attr = varcell.setncattr('cell_method', '${cellTime}')

# Cell Measure
##
##if not searchInlist(attvarcell, 'cell_measures'):
##  attr = varcell.setncattr('cell_measures', '${cellMeasure}')

attr = inc.setncattr('creation_date', '${dateval}')

inc.sync()
inc.close()
EOFpy
  
  python ${ROOTpost}/adding_TimeBounds.py

##  fileltimes=`echo ${filetimes} | tr ' ' '@'`
  
##  echo ${filetimes}
}

#######    #######
## MAIN
    #######
  netcdf=$1
  varname=$2
  wrfkind=$3
  freqname=$4
  namelistoutput=$5
  domain=$6
  RefDate=$7
  WRF4Ghome=$8
  period=$9
  

  echo "CF_CORDEX_cellmethods $netcdf $varname $wrfkind $freqname $namelistoutput $domain $WRF4Ghome"

  cellTimeMethod=`CF_CORDEX_cellmethods $netcdf $varname $wrfkind $freqname $namelistoutput $domain $WRF4Ghome`
  cellSpaceMethod=`CF_CORDEX_cellspace $netcdf $varname`
  
  refdate=${RefDate:0:4}${RefDate:5:2}${RefDate:8:2}${RefDate:11:2}${RefDate:14:2}${RefDate:17:2}
  add_time_bnds_cellmethod $netcdf $WRF4Ghome ${varname} ${freqname} ${refdate} ${cellTimeMethod} ${cellSpaceMethod} ${period}

fi
