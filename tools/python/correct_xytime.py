## Python script to modify lon, lat and time for the post-processed files
## e.g. # python correct_xytime.py -f CCRC_NARCliM_MOM_1950-1950_prac.nc -g 'NARCliM_domain!1$driving_wrf_schemes_cu!1: KF, Kain (2004, JAM)'
## e..g. # python correct_xytime.py -f CCRC_NARCliM_MOM_1950-1950_prac.nc -n /home/lluis/bin/gcc_nco-4.0.9/bin -g 'NARCliM_domain@1:driving_wrf_schemes_cu "1: KF, Kain (2004, JAM)"'

import numpy as np
from netCDF4 import Dataset as NetCDFFile
import os
from optparse import OptionParser

####### Functions
# searchInlist: searchs if a value is in a given list
# set_attribute: sets a value of an attribute of a netCDF variable
# ncks_create_unlimited_dim: sets a dimension as UNLIMITED using ncks. NOT USED
# create_unlimited_dim: sets a dimension as UNLIMITED

def searchInlist(listname, nameFind):
    for x in listname:
      if x == nameFind:
        return True
    return False

def set_attribute(ncvar, attrname, attrvalue):
    """ Sets a value of an attribute of a netCDF variable
    """
    import numpy as np
    from netCDF4 import Dataset as NetCDFFile

    attvar = ncvar.ncattrs()
    if searchInlist(attvar, attrname):
        attr = ncvar.delncattr(attrname)

    attr = ncvar.setncattr(attrname, attrvalue)

    return ncvar

def nco_create_unlimited_dim(ncoins, ncf, timen):
    """ sets a dimension as UNLIMITED using NCO. thanks to D. Argueso, CCRC-UNSW
    """
    import numpy as np
    from netCDF4 import Dataset as NetCDFFile
    import subprocess as sub

    errmsg='ERROR -- error -- ERROR -- error'

    newf = 'unlimit.nc'
    #unc = NetCDFFile(newf,'w')

    #dim = unc.createDimension('time', size=0)
    #unc.close()

    #ins = ncksins + ' -A ' + ncf + ' ' + newf
    ins1 = ncoins + '/ncecat -O ' + ncf + ' ' + newf
    ins2 = ncoins + '/ncpdq -O -a ' + timen +',record ' + newf + ' ' + newf
    ins3 = ncoins + '/ncwa -O -a record ' + newf + ' ' + newf

# 1st instruction. Add degenerate record dimension named "record"
    try:
        syscode = sub.call(ins1, shell=True)
##        syscode = sub.Popen([ins], stdout=sub.PIPE)

        if not syscode == 0:
            print errmsg
            print "  Execution of ncks failed!"
            print ins1
            print syscode
            quit()

    except OSError, e:
        print errmsg
        print "   Execution of ncks failed!"
        print e
        quit()

# 2nd instruction. Switch "record" and "time"
    try:
        syscode = sub.call(ins2, shell=True)
##        syscode = sub.Popen([ins], stdout=sub.PIPE)

        if not syscode == 0:
            print errmsg
            print "  Execution of ncks failed!"
            print ins1
            print syscode
            quit()

    except OSError, e:
        print errmsg
        print "   Execution of ncks failed!"
        print e
        quit()

# 3rd instruction. Remove (degenerate) "record" 
    try:
        syscode = sub.call(ins3, shell=True)
##        syscode = sub.Popen([ins], stdout=sub.PIPE)

        if not syscode == 0:
            print errmsg
            print "  Execution of ncks failed!"
            print ins1
            print syscode
            quit()

    except OSError, e:
        print errmsg
        print "   Execution of ncks failed!"
        print e
        quit()

    sub.call('mv ' + newf + ' ' + ncf, shell=True)

def searchInlist(listname, nameFind):
    for x in listname:
      if x == nameFind:
        return True
    return False

def create_unlimited_dim(ncf, dimn):
    """ sets a dimension as UNLIMITED
    """
    import numpy as np
    from netCDF4 import Dataset as NetCDFFile
    import subprocess as sub

    errmsg='ERROR -- error -- ERROR -- error'

    onc = NetCDFFile(ncf,'r')
    if not onc.dimensions[dimn].isunlimited():
        newf = 'unlimit.nc'
        unc = NetCDFFile(newf,'w')

        dim = unc.createDimension(dimn, size=0)
    
# Dimensions
## 
        for dim in onc.dimensions.values():
            dname = dim._name
            dsize = dim.__len__()
            undim = dim.isunlimited()
            if not dname == dimn:
                dim = unc.createDimension(dname, size=dsize)

# Variables
## 
        for var in onc.variables.values():
            vname = var._name
            vkind = var.dtype
            vdims = var.dimensions

            varattrs = var.ncattrs()
            if searchInlist(varattrs, '_FillValue'):
                fillval = var.getncattr('_FillValue')
                unvar = unc.createVariable(vname, vkind, vdims, fill_value=fillval)
            else:
                unvar = unc.createVariable(vname, vkind, vdims)

            unvar[:] = var[:]
            for vattr in var.ncattrs():
                if not vattr == '_FillValue':
                    vattrval = var.getncattr(vattr)
                    unvar = set_attribute(unvar, vattr, vattrval)

# Global attributes
##
        for gattr in onc.ncattrs():
            gattrval = onc.getncattr(gattr)
            unc = set_attribute(unc, gattr, gattrval)

        unc.sync()
        unc.close()
        try:
            ins = 'cp unlimit.nc ' + ncf
            syscode = sub.call(ins, shell=True)
    
            if not syscode == 0:
                print errmsg
                print "  Copy to the new file failed!"
                print ins
                print syscode
                quit()
            else:
                sub.call('rm unlimit.nc', shell=True)

        except OSError, e:
            print errmsg
            print "   Copy failed!"
            print e
            quit()
    
    onc.close()


####### ###### ##### #### ### ## #

parser = OptionParser()
parser.add_option("-g", "--global", dest="globalattr",
                  help="global attributes to add/modify (name1!val1$name2!val2)", metavar="DOMNUM")
parser.add_option("-f", "--file", dest="filename",
                  help="file to correct", metavar="FILENAME")
parser.add_option("-n", "--ncoIns", dest="ncoins",
                  help="path to the ncks bin folder", metavar="FILENAME")
(opts, args) = parser.parse_args()

#######    #######
## MAIN
    #######

ncfile = opts.filename

##create_unlimited_dim(ncfile, 'time')

nco_create_unlimited_dim(opts.ncoins, ncfile, 'time')

if not os.path.isfile(ncfile):
    print errormsg
    print '  File ' + ncfile + ' does not exist !!'
    print errormsg
    quit()    

inc = NetCDFFile(ncfile,'a')

# Correction of standard dimensions
##
lonv = inc.variables['lon']
Ndims=len(lonv.shape)
dimx = lonv.shape[Ndims-1]
dimy = lonv.shape[Ndims-2]

lonv = set_attribute(lonv, 'long_name', 'longitude')

latv = inc.variables['lat']
latv = set_attribute(latv, 'long_name', 'latitude')

timev = inc.variables['time']
timev = set_attribute(timev, 'long_name', 'time')
timev = set_attribute(timev, 'bounds', 'time_bnds')
inc.dimensions['time'] = None

if opts.globalattr is not None:
    # global attributes
    ##
    gattrs = opts.globalattr.split('$')
    for igattr in gattrs:
        attrvals = igattr.split('!')
        if not len(attrvals) == 2:
            print 'Attribute values are not correct! ', igattr
            quit()

        gattrn = igattr.split('!')[0]
        gattrv = igattr.split('!')[1]

        inc = set_attribute(inc, gattrn, gattrv)

print 'Adding x=',dimx,' y=',dimy

# Adding variable 'x'
## 
if not inc.variables.has_key('x'):
    print 'Creating x variable'
    xval = inc.createVariable('x', 'd', ('x'))
    xattr = xval.setncattr('axis', 'X')
    xattr = xval.setncattr('_CoordinateAxisType', 'GeoX')
else:
    xval = inc.variables['x']

xvalues = map(float, range(dimx))
xval[:] = xvalues

# Adding variable 'y'
## 
if not inc.variables.has_key('y'):
    print 'Creating y variable'
    yval = inc.createVariable('y', 'd', ('y'))
    yattr = yval.setncattr('axis', 'Y')
    yattr = yval.setncattr('_CoordinateAxisType', 'GeoY')
else:
    yval = inc.variables['y']

yvalues = map(float, range(dimy))
yval[:] = yvalues

inc.sync()
inc.close()
