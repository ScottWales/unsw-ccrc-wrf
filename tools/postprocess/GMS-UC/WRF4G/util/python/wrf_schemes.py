## g.e. # python WRF4G/util/python/wrf_schemes.py -w WRF_schemes.inf -f /home/lluis/studies/NARCliMGreatSydney2km/data/wrfout_d01_1997-02-01_00:00:00 -s 'RA_LW_PHYSICS:RA_SW_PHYSICS:SF_SFCLAY_PHYSICS:SF_SURFACE_PHYSICS:BL_PBL_PHYSICS:CU_PHYSICS'

from optparse import OptionParser
import os
from netCDF4 import Dataset as NetCDFFile
from pprint import pprint

parser = OptionParser()
parser.add_option("-f", "--WRFfile", dest="WRFfile", 
                  help="WF output file to use", metavar="FILE")
parser.add_option("-s", "--WRFschemes", dest="schemes", 
                  help="schemes to check names as appear as global attributes [sch1]:...:[schn]", metavar="VAR")
parser.add_option("-w", "--WRFschemesASCII", dest="WRFsf",  
                  help="ASCII file with WRF references", metavar="FILE")

(opts, args) = parser.parse_args()

def searchInlist(listname, nameFind):
    for x in listname:
      if x == nameFind:
        return True
    return False

def list_char(listv):
    """Function to remove all non value characters from a list value transformed to string
    >>> list_char(str(['Lluis Fita']))
    Lluis Fita
    """
    listvalue = listv.replace('[', '')
    listvalue = listvalue.replace(']', '')
    return listvalue

def dictionary2entries(vals1, vals2, vals3):
    """ Function to create a dictionary with 2 entries (thanks to Jeff Exbrayat, CoECSCC-CCRC)
    >>>country = ['India', 'Germany', 'Guatemala', 'Burma', 'South Africa', 'South Africa']
    >>>person = ['Mohandas Karamchand Ghandi', 'Albert Einstein', 'Rigoberta Menchu', 'Aung San Suu Kyi', 'Nelson Mandela', 'Desmond Tutu']
    >>>fight = ['Independence', 'Nuclear bomb', 'Indigenous Rights', 'Freedom', 'anti-Apartheid', 'Reconciliation']

    >>>dict3 = dictionary3entries(country, person, fight)

    >>>print dict3['South Africa']['Nelson Mandela']
    anti-Apartheid
    >>>print dict3['South Africa']
    {'Nelson Mandela': 'anti-Apartheid', 'Desmond Tutu': 'Reconciliation'}
    """

    dicjeff={}

    for ii in range(len(vals1)):
##        print vals1[ii], vals2[ii], vals3[ii]

        if vals1[ii] not in dicjeff.keys():
            dicjeff[vals1[ii]]={}
            if vals2[ii] not in dicjeff[vals1[ii]].keys():
                dicjeff[vals1[ii]][vals2[ii]]={}
                dicjeff[vals1[ii]][vals2[ii]]=vals3[ii]
            else:
                dicjeff[vals1[ii]][vals2[ii]]=vals3[ii]
        else:
            if vals2[ii] not in dicjeff[vals1[ii]].keys():
                dicjeff[vals1[ii]][vals2[ii]]={}
                dicjeff[vals1[ii]][vals2[ii]]=vals3[ii]
            else:
                dicjeff[vals1[ii]][vals2[ii]]=vals3[ii]

    return dicjeff


#######    #######
## MAIN
    #######

####### ###### ##### #### ### ## #
errormsg='wrf_scheme.py: ERROR -- error -- ERROR -- error'

if not os.path.isfile(opts.WRFfile):
    print errormsg
    print '   WRF output file "' + opts.WRFfile + '" does not exist !!'
    print errormsg
    quit()    

if not os.path.isfile(opts.WRFsf):
    print errormsg
    print '  ASCII WRF scheme file "' + opts.WRFsf + '" does not exist !!'
    print errormsg
    quit()    

schWRF = open(opts.WRFsf, 'r')

sch_kind = []
sch_num = []
sch_val = []

for line in schWRF:
    values=line.split(' ')
    kind = values[1]

    sch_kind.append(kind)
    sch_num.append(int(values[2]))
    sch_val.append(values[3] + ': ' + values[4].replace('!', ' ').replace('\n',''))

schWRF.close()

# Filling dictionaries
##
sch_wrf = {}

sch_wrf = dictionary2entries(sch_kind, sch_num, sch_val)

# Looking for values
##
sch_name = []
sch_val = []

schemes = opts.schemes.split(':')
ncf = NetCDFFile(opts.WRFfile,'r')
gattr = ncf.ncattrs()

for isch in schemes:
    if not searchInlist(gattr, isch):
        print errormsg
        print '  File "' + opts.WRFfile + '" does not have global attribute "' + isch + '" !!!!'
        ncf.close()
        quit()      
    else:
        sch_name.append(isch.lower())
        sch_val.append(ncf.getncattr(isch))

ncf.close()

sch={}
sch=dict(zip(sch_name, sch_val))

wrf_config = {}
for ival in sch:
    if sch[ival] not in sch_wrf[ival].keys():
        print errormsg
        print '  WRF ASCII file "' + opts.WRFsf + '" does not have ' + ival + ' = ' + str(sch[ival])
    else:
        wrf_config[ival] = str(sch[ival]) + ', ' + sch_wrf[ival][sch[ival]]

pprint(wrf_config)

ofile = open('wrf.conf', 'w')
for val in wrf_config:
    ofile.write("driving_wrf_schemes_" + val + ' "' + wrf_config[val] + '"\n')

ofile.close()
print '## WRF configuration written in "wrf.conf"'


