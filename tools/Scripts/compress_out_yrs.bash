#!/bin/bash 
# NARCliM R3 e.g. # nohup ./compress_out.bash 1951 /srv/ccrc/data12/z3263184/NARCliM/nnrp/R3/out R3 &
# NARCliMGreatSydney nnrp e.g. # nohup ./compress_out.bash 1992 /srv/ccrc/data11/z3236814/NARCliM/Sydney/nnrp/out syd_nnrp_ctl & 
# NARCliMgreatSydney nnrp2 e.g. # nohup ./compress_out.bash 1995 /srv/ccrc/data13/z3273429/NARClim/Sydney/wrf/nnrp/out syd_nnrp2_ctl &
if test $1 = '-h'; then

  echo "***********************"
  echo "*** Shell script to ***"
  echo "***   to compress   ***"
  echo "***  wrf out files  ***"
  echo "*** of a given year ***"
  echo "***********************"
  echo "compress_wrf.bash [start year](YYYY, format) [end year] [folder(absolute path)] [label]"
else
  rootsh=`pwd`
  if test $# -lt 3; then
    echo "ERROR -- error -- ERROR -- error "
    echo "  A unique year is needed!!!"
    echo " a folder name is needed!!!"
    echo " a label is needed !!!"
    echo "  and you provide '"$*"'"
    echo "ERROR -- error -- ERROR -- error "
    exit
  fi


  sYEAR=$1
  eYEAR=$2
  Yidir=$3
  label=$4
  logfile=${rootsh}/compression_${label}_${iwrfYEAR}.log
  rm ${logfile} >& /dev/null
  echo "Compressing NARCliM "${label}" year='"${iwrfYEAR}"'" > ${logfile} 
  date >> ${logfile} 2>&1

  module -l list >& module.inf
  modintel=`cat module.inf | grep 'intel/' | awk '{print $1}'`
  modhdf=`cat module.inf | grep 'hdf5/' | awk '{print $1}'`
  modnetcdf=`cat module.inf | grep 'netcdf/' | awk '{print $1}'`
  goodintel='intel/11.1.080'
  goodhdf='hdf5/1.8.8-intel'
  goodnetcdf='netcdf/4.1.3-intel'

  Lmodintel=`expr length ${modintel}'A'`
  if test $Lmodintel -gt 1; then
    if test ! ${modintel} = ${goodintel}; then
      module unload ${modintel}
      module load ${goodintel}
    fi
  else 
    module load ${goodintel}
  fi

  Lmodhdf=`expr length ${modhdf}'A'`
  if test $Lmodhdf -gt 1; then
    if test ! ${modhdf} = ${goodhdf}; then
      module unload ${modhdf}
      module load ${goodhdf}
    fi
  else
    module load ${goodhdf}
  fi

  Lmodnetcdf=`expr length ${modnetcdf}'A'`
  if test $Lmodnetcdf -gt 1; then
    if test ! ${modnetcdf} = ${goodnetcdf}; then
      module unload ${modnetcdf}
      module load ${goodnetcdf}
    fi
  else
    module load ${goodnetcdf}
  fi

# Yi's folder
##  Yidir='/srv/ccrc/data12/z3263184/NARCliM/nnrp/R3/out'

  daten=`date +%Y%m%d%H%M%S`
  workdir=${Yidir}'/tmpdir_'${daten}
  mkdir ${workdir}
  cd ${workdir}
  echo "working dir: '"${workdir}"' ... .. ." >> ${logfile} 2>&1

#
# Compression of files
 iwrfYEAR=${sYEAR}
 while test ${iwrfYEAR} -le ${eYEAR}; do
 
  imon=1
  while test ${imon} -le 12; do
    month=`printf "%02d" ${imon}`
    echo ${datevalue}"-- Year: "${iwrfYEAR}" month: "${month}

    Nfiles=`ls -1 ${Yidir}/wrf*_${iwrfYEAR}-${month}-* | wc -l`
    if test $Nfiles -gt 1; then
      for file in ${Yidir}/wrf*_${iwrfYEAR}-${month}-*; do 
        mv $file ${workdir}/wrfout.nc >> ${logfile} 2>&1
        fac=$?
        if test $fac -ne 0; then
          echo "'"$file"' does not exist!!!!" 
          exit $fac
        fi
        nccopy -d 9 ${workdir}/wrfout.nc ${file} >> ${logfile} 2>&1 
        nc=$?
        if test $nc -ne 0; then
          echo "nccopy does not work!!!!" 
          mv ${workdir}/wrfout.nc $file >> ${logfile} 2>&1
          exit $nc
        fi
        rm ${workdir}/wrfout.nc >> ${logfile} 2>&1
#        exit
      done # end of files
    fi # end of files presence
    imon=`expr ${imon} + 1`
#    exit
  done # end of months
  
 iwrfYEAR=`expr ${iwrfYEAR} + 1`
 done # end of years
 
  cd ${rootsh}
  rmdir ${workdir}
  echo "*****************************" >> ${logfile} 2>&1
  echo "*** PROGRAM FINISHED !!!! ***" >> ${logfile} 2>&1
  echo "*****************************" >> ${logfile} 2>&1
  date >> ${logfile} 2>&1
fi
