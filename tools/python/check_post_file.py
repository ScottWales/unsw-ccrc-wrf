import numpy as np
from optparse import OptionParser
import os
from netCDF4 import Dataset as NetCDFFile
from scipy.stats.stats import pearsonr
import nc_var_tools as ncvar
import subprocess as sub

errormsg='check_post_file.py: ERROR -- error -- ERROR -- error'
## g.e. # check_post_file.py -f 123_pracc/CCRC_NARCliM_DAM_2030-2039_pracc.nc -v pracc -t 24

def UseFormat(val,form):
    """ Function that produces the output giving a format specification
    """
    Lform=len(form)
    fwidth=form[0:Lform-1]
    ftype=form[Lform-1:Lform]
    if form[0:1] == '0':
        ffill=0
    else:
        ffill=''

    if ftype == 'c':
        fval=str(val)
    elif ftype == 'd':
        fval=int(val)
    elif ftype == 'f':
        fval=float(val)
    else:
        print errmsg
        print '  "' + ftype + '" format specification not ready!!!'
        quit(-1)

##    print ffill, fwidth, ftype

    return '{num:{fill}{width}{type}}'.format(num=fval, fill=ffill, width=fwidth, type=ftype)

def matrix2string(mat, chars, forms):
    """ Function to generate a string chain from a matrix with a given list of characters between each column value and another between values
    and a series of formats for each column value
    >>>matrix2string( np.array( [(2.3, 3.56, 1.3), (23.2, 1231.1, -123.1)]), ['--', '@@', ' '], ['.3f', '.9f', '06d'])
    2.300--3.560000000@@000001 23.200--1231.100000000@@-00123
    """
    stringchain=''
    dims=mat.shape
    dimr=dims[0]
    dimc=dims[1]
    if not len(chars) == dimc:
        print errormsg
        print '  number of columns: ', dimc, ' do not coincide with the number of characters ', len(chars), ' provided !'
        quit(-1)
    if not len(forms) == dimc:
        print errormsg
        print '  number of columns: ', dimc, ' do not coincide with the number of formats ', len(forms), ' provided !'
        quit(-1)

    matend=mat[dimr-1,0]
    for ir in range(dimr):
        for ic in range(dimc-1):
            stringchain=stringchain + UseFormat(mat[ir,ic], forms[ic]) + chars[ic]

        if ir == dimr-1:
            stringchain=stringchain + UseFormat(mat[ir,dimc-1], forms[dimc-1])
        else:
            stringchain=stringchain + UseFormat(mat[ir,dimc-1], forms[dimc-1]) + chars[dimc-1]

    return stringchain

#######    ########
## MAIN
    #######
parser = OptionParser()

parser.add_option("-f", "--file", dest="filename", 
                  help="file to check", metavar="FILENAME")
parser.add_option("-t", "--timestep", dest="tstep",
                  help="theoretical time-step (hours) between content", metavar="TIMEVALUE")
parser.add_option("-v", "--variable", dest="varname",
                  help="variable in the file", metavar="VARNAME")

(opts, args) = parser.parse_args()

####### ###### ##### #### ### ## #

if not os.path.isfile(opts.filename):
    print errormsg
    print '  post-processed file "' + opts.filename + '" does not exist !!'
    print errormsg
    quit(-1)    

ncf = NetCDFFile(opts.filename,'r')

if not ncf.variables.has_key(opts.varname):
    print errormsg
    print '  File does not have variable ' + opts.varname + ' !!!!'
    ncf.close()
    quit(-1)

var = ncf.variables[opts.varname]

# Checking times
##
print '  Checking times...'
timev = ncf.variables['time']
times = timev[:]
dimt = times.shape[0]

datetimes = ncvar.CFtimes_datetime(ncf, 'time')
timeInformation = ncvar.time_information(ncf, 'time')

if timeInformation[0] == 'days':
    timestep = float(opts.tstep / 24)
elif timeInformation[0] == 'hours':
    timestep = float(opts.tstep)
elif timeInformation[0] == 'minutes':
    timestep = float(opts.tstep * 60) 
elif timeInformation[0] == 'seconds':
    timestep = float(opts.tstep * 3600)
else:
    print errmsg
    print '  "', timeInformation[0],'" not ready!'
    quit(-1)

dtimes = np.array((dimt-1), dtype=float)
dtimes = times[1:dimt] - times[0:dimt-1]

for it in range(dimt-1):
    #print it
    if not dtimes[it] == timestep:
        print errormsg
        print '  No correct time-step between values !'
        print '  It should be:', timestep,' but it is:', dtimes[it]
        dates=matrix2string(datetimes[it:it+2,:], ['/', '/', ' ', ':', ':', '@'], ['4d', '02d', '02d', '02d', '02d', '02d']).split('@')
        print '  previous: ', dates[0] 
        print '  next: ', dates[1] 
        quit(-1)
