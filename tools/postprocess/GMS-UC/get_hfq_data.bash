#!/bin/bash
if test $1 = '-h'; then
  echo "*************************"
  echo "***  Shell to produce ***"
  echo "***      CF files     ***"
  echo "*** from wrfout files ***"
  echo "*************************"
  echo "get_hfq_data.bash [environfile] [expname] [exppath] [variable] [outkind] [syearmon] [eyearmon] [CFvar]"
  echo "  [environfile]: file with the environment of the postprocess "
  echo "  [expname]: name of the experiment to assign as header of the files "
  echo "  [exppath]: folder with the experiment "
  echo "  [variable]: name of the variable "
  echo "  [outkind]: kind of WRF-ARW output ('out', 'xtrm', 'hly', 'dly') "
  echo "  [syearmon]: starting year of the period "
  echo "  [eyearmon]: ending year of the period "
  echo "  [varLEV]: level of the variable ('SFC', surface)"
  echo "  [CFvar]: CF name of the variable "
else

####### Functions
# replace_substring: Shell to replace a substring for another one within a string
# isin_list: Function to check whether a value is in a list
# get_yearmons: get the all the dates of existing files within a year month period
# get_filelist_yearmon: gets a list of files within a year month period
# get_filefirst_yearmon: gets the first file of a year month period
# get_filelast_yearmon: gets the last file of a year month period
# check_accumulate: Function to check whether a variable should be deaccumualted
# filelist_is_short: Checks if a list of files has enough files according to a year month period
# next_yearmon: Function to obtain next month from a [yyyy][mm] structure
# prev_yearmon: Function to obtain previous month from a [yyyy][mm] structure
# TimeSteps_checking: Function to check timesteps content of an output file from a 'filelist.txt' file. Assuming WRF files
#   with Times as name of the time dimension
# get_Ntimes_namelist: Function to get Ntimes from a namelist.output of a given kind of wrf file and a given domain
# seconds_HMS: Function to transform from seconds to Hour, minutes and seconds
# check_stats_varn: Function to check if variable name has a statistic section

function replace_substring() {
# Shell to replace a substring for another one within a string
  string=$1
  ssubstring=$2
  rsubstring=$3
  
  wheresub=`echo ${string}" "${ssubstring} | awk '{print index($1,$2)}'`
  wheresub1=`expr ${wheresub} - 1`
  Lssubstring=`expr length ${ssubstring}`
  Lrsubstring=`expr length ${rsubstring}`
  Lstring=`expr length ${string}`
  Lendssubstring=`expr ${wheresub} + ${Lssubstring}`
  Lendnewstring=`expr $Lstring - ${Lendssubstring} + 1`
  begnewstring=`expr substr ${string} 1 ${wheresub1}`
  endnewstring=`expr substr ${string} ${Lendssubstring} ${Lendnewstring}`
  newstring=${begnewstring}${rsubstring}${endnewstring}
  
  echo ${newstring}
}

function isin_list() {
# Function to check whether a value is in a list
  list=$1
  value=$2
  
  is=`echo ${list} | tr ':' '\n' | awk '{print "@"$1"@"}' | grep '@'${value}'@' | wc -w`
  if test ${is} -eq 1
  then
    true
  else
    false
  fi
}

function get_yearmons(){
  idir=$1
  okind=$2
##  find ${idir}/output/*/ -name wrf${okind}_d01_\*.nc \
  find ${idir} -name wrf${okind}_d0${DOMAINsim}_\* \
    | sed -e 's/^.*wrf'${okind}'_d0'${DOMAINsim}'_\(.*\)............$/\1/' | sed 's/\-//' \
    | sort -n | uniq
}

function get_filelist_yearmon() {
  exppath=$1
  ymon=$2
  filekind=$3
  YMy=`expr substr ${ymon} 1 4`
  YMm=${ymon:4:2}
  overlap=''
  if $(isin_list ${overlap_years} ${YMy})
  then
    if test ${yearmon} -le $(echo ${overlap_yearmon} | tr ':' '\n' | grep ${YMy})
    then
      overlap='f'
    else
      overlap='i'
    fi
    find ${exppath}/*${overlap} -name wrf${filekind}_d0${DOMAINsim}_${YMy}-${YMm}\* | sort
  else
    find ${exppath} -name wrf${filekind}_d0${DOMAINsim}_${YMy}-${YMm}\* | sort
  fi
##  find ${exppath}/*${overlap} -name wrf${filekind}_d01_${ymon}\*.nc | sort
#WorkingOne#  find ${exppath}/*${overlap} -name wrf${filekind}_d0${DOMAINsim}_${YMy}-${YMm}\* | sort

}

function get_filefirst_yearmon() {
  exppath=$1
  ymon=$2
  filekind=$3
  YMy=`expr substr ${ymon} 1 4`
  YMm=${ymon:4:2}
  overlap=''
  if $(isin_list ${overlap_years} ${YMy})
  then
    overlap='i'
    find ${exppath}/*${overlap} -name wrf${filekind}_d0${DOMAINsim}_${YMy}-${YMm}\* | sort | head -n 1
  else
    find ${exppath} -name wrf${filekind}_d0${DOMAINsim}_${YMy}-${YMm}\* | sort | head -n 1
  fi
##  find ${exppath}/*${overlap} -name wrf${filekind}_d01_${ymon}\*.nc | sort | head -n 1
}

function get_filelast_yearmon() {
  exppath=$1
  ymon=$2
  filekind=$3
  YMy=`expr substr ${ymon} 1 4`
  YMm=${ymon:4:2}
  overlap=''
  if $(isin_list ${overlap_years} ${YMy})
  then
    overlap='f'
    find ${exppath}/*${overlap} -name wrf${filekind}_d0${DOMAINsim}_${YMy}-${YMm}\* | sort | tail -n 1
  else
    find ${exppath} -name wrf${filekind}_d0${DOMAINsim}_${YMy}-${YMm}\* | sort | tail -n 1
  fi
##  find ${exppath}/*${overlap} -name wrf${filekind}_d01_${ymon}\*.nc | sort | tail -n 1
}

function check_accumulate() {
# Function to check whether a variable should be deaccumualted
  varname=$1
  wrfncxnjtable=$2

  linvalue=`cat -n ${wrfncxnjtable} | awk '{print " "$2" "$1}' | grep ' '${varname}' ' | awk '{print $2}'`
  deaccum=`head -n ${linvalue} ${wrfncxnjtable} | tail -n 1 | grep deaccumulate | wc -m`
  if test ${deaccum} -gt 0
  then
    true
  else
    false
  fi
}

function filelist_is_short(){
  flfile=$1
  ymon=$2
  s1=$(date --utc '+%s' -d "${ymon:0:4}-${ymon:4:2}-01 1 month")
  s2=$(date --utc '+%s' -d "${ymon:0:4}-${ymon:4:2}-01")
  daysinmonth=$(python -c "print ($s1-$s2)/60/60/24")
  test "${daysinmonth}" -ne $(cat ${flfile} | wc -l)
}

function next_yearmon() {
# Function to obtain next month from a [yyyy][mm] structure
  ymon=$1
  year=`expr substr ${ymon} 1 4`
  month=`expr substr ${ymon} 5 2`

  nextmon=`expr $month + 1`
  nextmonS=$nextmon
  if test $nextmon -lt 10
  then
    nextmonS='0'${nextmon}
  fi

  if test ${nextmon} -gt 12
  then
    nextmonS='01'
    nextyear=`expr ${year} + 1`
    echo ${nextyear}${nextmonS}" year_changed"
  else
    echo ${year}${nextmonS}
  fi
}

function prev_yearmon() {
# Function to obtain previous month from a [yyyy][mm] structure
  ymon=$1
  year=`expr substr ${ymon} 1 4`
  month=`expr substr ${ymon} 5 2`

  prevmon=`expr $month - 1`
  prevmonS=$prevmon
  if test $prevmon -lt 10
  then
    prevmonS='0'${prevmon}
  fi

  if test ${prevmon} -lt 1
  then
    prevmonS='12'
    prevyear=`expr ${year} - 1`
    echo ${prevyear}${prevmonS}" year_changed"
  else
    echo ${year}${prevmonS}
  fi
}

function TimeSteps_checking() {
# Function to check timesteps content of an output file from a 'filelist.txt' file. Assuming WRF files
#   with Times as name of the time dimension
  filelst=$1
  ofilechk=$2
  timen=$3

  if test ! -f ${ofilechk}; then
    echo "'"${ofilechk}"' does not exist !!!!"
    exit
  fi

  refdateYmdHMS=${RefDate:0:4}${RefDate:5:2}${RefDate:8:2}${RefDate:11:2}${RefDate:14:2}${RefDate:17:2}

  fileNsteps=`python ${WRF4Ghome}/util/python/nc_time_tools.py -f ${ofilechk} -t time -r ${refdateYmdHMS} -F 1 -o timeINF | awk '{print $1}'`

  files=`cat ${filelst}`
  NTOTtimes=0
  for ifile in ${files}; do
    fileNtimes=`python ${WRF4Ghome}/util/python/nc_time_tools.py -f ${ifile} -t Times -r ${refdateYmdHMS} -F 1 -o timeINF | awk '{print $1}'`
##    fileNtimes=`${WRF4Ghome}/util/Fortran/nc_file_inf ${ifile} | grep -a --text DIMinf | grep Time | awk '{print $4}'`
    NTOTtimes=`expr ${NTOTtimes} + ${fileNtimes}`
  done # 
  if test ${fileNsteps} -ne ${NTOTtimes}; then
    echo "ERROR -- error -- ERROR -- error"
    echo "  Outputed file '"${ofilechk}"' have "${fileNsteps}" time steps and it should have "${NTOTtimes}" !!!"
    echo "ERROR -- error -- ERROR -- error"
#    exit
  fi
}

get_Ntimes_namelist(){
# Function to get Ntimes from a namelist.output of a given kind of wrf file and a given domain
#
### Variables
# namlst: 'namelist.output' to use to find the 
# wrfdom: wrf domain
# fk: wrf kind of file 'out', 'xtrm', 'dly', 'hrly',... (must much with value in namelist.output as 'AUXHIST[NN]_OUTNAME')

  namlst=$1
  wrfdom=$2
  fk=$3

  auxhist=`cat ${namlst} | grep ${fk} | awk '{print $1}' | tr '_' ' ' | awk '{print $1}'`
  histfrq=`cat ${namlst} | grep ${auxhist}_INTERVAL | grep -v _[YDHMS] ` # Assuming that [AUXHIST]_INTERVAL_[Y/D/H/M/S] is not used
# interval
#
  nvalues=`echo $histfrq | awk '{print $3}' | tr '*' ' ' | awk '{print $1}'`
  if test ${nvalues} -ge $wrfdom; then
    histfrq=`echo $histfrq | awk '{print $3}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
  else
    histfrq=`echo $histfrq | awk '{print $4}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
  fi
# frames x file
# 
  if test ${fk} = 'out'; then
    nvalues=`cat ${namlst} | grep FRAMES_PER_OUTFILE | awk '{print $3}' | tr '*' ' ' | awk '{print $1}'`
    if test  ${nvalues} -ge $wrfdom; then
      framesfile=`cat ${namlst} | grep FRAMES_PER_OUTFILE | awk '{print $3}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
    else
      framesfile=`cat ${namlst} | grep FRAMES_PER_OUTFILE | awk '{print $4}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
    fi
  else
    nvalues=`cat ${namlst} | grep FRAMES_PER_${auxhist} | awk '{print $3}' | tr '*' ' ' | awk '{print $1}'`
    if test  ${nvalues} -ge $wrfdom; then
      framesfile=`cat ${namlst} | grep FRAMES_PER_${auxhist} | awk '{print $3}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
    else
      framesfile=`cat ${namlst} | grep FRAMES_PER_${auxhist} | awk '{print $4}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
    fi
  fi

# Internal time_step (seconds)
#
  timeStepSec=`cat ${namlst} | grep ' TIME_STEP ' | awk '{print $3}' | tr ',' ' '`
  echo $histfrq" "$framesfile" "$timeStepSec
}

seconds_HMS() {
# Function to transform from seconds to Hour, minutes and seconds
  secs=$1

  Hour=`expr ${secs} / 3600 | awk '{printf("%02d",$1)}'`
  Min=`echo ${secs}" "${Hour}" 3600 60" | awk '{printf("%02d",($1-$2*$3)/$4)}'`
  Sec=`echo ${secs}" "${Hour}" "${Min}" 3600 60" | awk '{printf("%02d",$1-$2*$4-$3*$5)}'`

  echo ${Hour}" "${Min}" "${Sec}
}

function check_stats_varn() {
# Function to check if variable name has a statistic section
  varN=$1
  LvarN=`expr length ${varN}`
  statsnames='min max mean ac mintstep maxtstep meantstep actstep'

  statsvar='0'
  for statsn in ${statsnames}; do
    Lstatsn=`expr length ${statsn}`
    Lvarsec=`expr $LvarN - $Lstatsn`
    if test ${Lvarsec} -gt 1; then
      varsec=${varN:${Lvarsec}:${Lstatsn}}
      if test ${varsec} = ${statsn}; then
#
# Removing 'tstep' surname
        if test $Lstatsn -gt 4; then
          Llvarsec=`expr length $varsec`
          LvarsecNOtstep=`expr $Llvarsec - 5`
          varsec=${varsec:0:${LvarsecNOtstep}}
        fi
        statsvar=${varsec}
      fi
    fi
  done # end of statsnames
  echo ${statsvar}
}

pressure_interp (){
# Function to vertically pressure interpolate the WRF outputs
  oripath=$1
  outkind=$2
  outpath=$3
  WRF4Gh=$4
  postf=$5
  auxk=$6

  errmsg='ERROR -- error -- ERROR -- error'

  heredir=`pwd`

  refdateYmdHMS=${RefDate:0:4}${RefDate:5:2}${RefDate:8:2}${RefDate:11:2}${RefDate:14:2}${RefDate:17:2}

  mkdir -p ${outpath}
  cd ${outpath}

  rawfiles=`ls -1 ${oripath}/wrf${outkind}*_d0${DOMAINsim}_*`

  for file in ${rawfiles}; do
    filen=`basename ${file}`
    if test ! -f ${outpath}/${filen}; then
      echo "'${filen}' was not interpolated at running time ...Doing it now!"
      if test -d ${outpath}/not_filtered
      then
        rm ${outpath}/not_filtered/*
      else
        mkdir -p ${outpath}/not_filtered/  
      fi
#LL#          cp $f ${indir}/not_filtered
      ln -s ${oripath}/${filen} ${outpath}/not_filtered
#
# Do we need auxiliar files?      
      if ! test ${auxk} = 'null'; then
        rm ${outpath}/not_filtered/wrf${auxk}* >& /dev/null
        ln -s ${oripath}/wrf${auxk}_d0${DOMAINsim}_* ${outpath}/not_filtered
        auxfiles=`python ${WRF4Gh}/util/python/auxfiles_4file.py -f ${filen} -d ${outpath}/not_filtered -k \
          ${auxk} -D ${DOMAINsim} -r ${refdateYmdHMS} -w ${WRF4Gh}/util/python`
        Nauxfiles=`echo ${auxfiles} | wc -w`
        auxfiles=`echo ${auxfiles} | tr ' ' ','`
        if test ${Nauxfiles} -lt 1; then
          echo ${errmsg}
          echo "  It is supposed that we should have auxiliar '"${auxk}"' files but we do not have any!"
          echo ${errmsg}
        fi
      fi
      ${WRF4Ghome}/wn/bin/postprocessor.${postprocessor} ${WRF4Gh} ${outpath}/not_filtered ${filen}        \
        ${outpath} ${Nauxfiles} ${auxfiles}
      if ! test ${auxk} = 'null'; then rm ${outpath}/not_filtered/wrf${auxk}* >& /dev/null; fi

    fi
  done # end of the raw files

  rm -rf ${outpath}/not_filtered
  cd ${heredir}

}

function isLeap {
# Function to test if a year is a leap year
  year=$1

  nextday=`date +%d -d"${year}0228 1 day"`
  if test ${nextday} -eq 29; then
    return 0
  else
    return 1
  fi
}

#######    #######
## MAIN
    #######

  environfile=$1
  expname=$2
  exppathi=$3
  variable=$4
  outkindi=$5
  syearmon=$6
  eyearmon=$7
  varLEV=$8
  CFvar=$9

  makelevs=''
##  if test ${outkind} = 'out' && test ! $varLEV = 'SFC'
  if test ! $varLEV = 'SFC'
  then
    if test ${varLEV} = 'GROUND'; then
      makelevs='-s'
    else
      makelevs='-p'
    fi
  else
    makelevs='-z'
  fi

# Checking for pressure interpolated values
# 
  kindp=${outkindi:0:2}
  interp=false
  if test ${kindp} == 'p@'; then
    interp=true
    Loutkindi=`expr length ${outkindi}`
    Loutkindi2=`expr ${Loutkindi} - 2`
    outkind=${outkindi:2:${Loutkindi}}
    isamp=`expr index ${outkind} '&'`
    if test ${isamp} -ne 0; then
      auxkind=`echo ${outkind} | tr '&' ' ' | awk '{print $2}'`
      outkind=`echo ${outkind} | tr '&' ' ' | awk '{print $1}'`
      echo "Processing variable '"${variable}"' from '"${outkind}                                \
        "' pressure interpolated WRF files using '"${auxkind}"' as auxiliar files"
    else
      auxkind='null'
      echo "Processing variable '"${variable}"' from '"${outkind}"' pressure interpolated WRF files"
    fi
    exppath=${EXPinterpDIR}
    Nfiles=`ls -1 ${exppath}/wrf${outkind}*_d0${DOMAINsim}_* | wc -l | awk '{print $1}'`
    Nrawfiles=`ls -1 ${exppathi}/wrf${outkind}*_d0${DOMAINsim}_* | wc -l | awk '{print $1}'`
    if test ! ${Nfiles} -eq ${Nrawfiles}; then
#
## computing pressure interpolated all files
      pressure_interp ${exppathi} ${outkind} ${exppath} ${WRF4Ghome} ${postprocessor} ${auxkind}
    fi
  else
    outkind=${outkindi}
    echo "Processing variable '"${variable}"' from '"${outkind}"' WRF files"
    exppath=${exppathi}
  fi

  for yearmon in $(get_yearmons ${exppath} ${outkind}); do
    if test ${yearmon} -ge ${syearmon} && test ${yearmon} -le ${eyearmon}
    then
      outfile="${POSTDIR}/${expname}_${yearmon}.nc"
      if test -f ${outfile}; then
        echo "I won't overwrite ${outfile}."
        continue
      fi
      prevyearmon=`prev_yearmon ${yearmon}`
      nextyearmon=`next_yearmon ${yearmon}`
#
# Checking overlapping period
#
      YMy=`expr substr ${yearmon} 1 4`
      if $(isin_list ${overlap_years} ${YMy})
      then
        echo "Desired year: "${YMy}" is an overlapping year!"
      fi 
      
      rm -f filelist.txt
      if test ! ${outkind} = 'out'
      then
        get_filelast_yearmon ${exppath} $(echo ${prevyearmon} | awk '{print $1}') ${outkind} > filelist.txt  
      fi

      get_filelist_yearmon ${exppath} ${yearmon} ${outkind} \
        | while read f; do
#        if ncdump -h ${f} | grep -q num_metgrid_levels && test ${outkind} = 'out' ; then
#LL#        if ! ncdump -h ${f} | grep -q num_metgrid_levels && test ${outkind} = 'out' ; then
#
#          echo "$f was not filtered at running time"
#          echo "Filtering now!"
#          indir=$(dirname $f)
#          infile=$(basename $f)
#          if test -d ${indir}/not_filtered
#          then
#            rm ${indir}/not_filtered/*
#          else
#            mkdir ${indir}/not_filtered/  
#          fi
#LL#          cp $f ${indir}/not_filtered
#          ln -s $f ${indir}/not_filtered
#          ${WRF4Ghome}/ui/scripts/wrf4g_run_postprocessor.sh ${WRF4Ghome} ${postprocessor} \
#            ${indir}/not_filtered
#          mv ${indir}/not_filtered/${infile} ${indir}
#          echo ${f} >> filelist.txt
#        else
          echo ${f} >> filelist.txt
#        fi
      done

      if test ! ${outkind} = 'out'
      then
        get_filefirst_yearmon ${exppath} $(echo ${nextyearmon} | awk '{print $1}') ${outkind} >> filelist.txt
      fi
      if filelist_is_short filelist.txt ${yearmon} && test ${outkind} = 'out' ; then
        echo "Too few files for month ${yearmon:4:2}: $(wc -l filelist.txt)"
        cp filelist.txt filelist_${yearmon}.txt
        #continue
      fi
# Checking if variable is accumulative one
#
      if $(check_accumulate ${variable} ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table)
      then
        noprevyear=0
        nwords=`echo $prevyearmon | wc -w`
        if test ${nwords} -gt 1
        then
          yearprev=`echo ${prevyearmon} | awk '{print substr($1,1,4)}'`
          if test ${yearprev} -ge ${FirstEXPy}
          then
            indir=`dirname $(tail -n 1 filelist.txt)`
            prevfile=`get_filelast_yearmon ${exppath} $(echo ${prevyearmon} | awk '{print $1}') ${outkind}` 
          else
            noprevyear=1
          fi
        else
          ##prevfile=`tail -n 1 filelist_${prevyearmon}.txt` 
          prevfile=`get_filelast_yearmon ${exppath} $(echo ${prevyearmon} | awk '{print $1}') ${outkind}` 
          if $(isin_list ${overlap_yearmon} ${prevyearmon})
          then
            prevyear=`expr substr 1 4 ${prevyearmon}`
            prevfile=`replace_substring ${prevfile} ${prevyear}'f' ${prevyear}'i'`
          fi
        fi
        
        if test ${noprevyear} -eq 0
        then
          ${wxajcmd} \
            --from-file filelist.txt \
            -o ${outfile} \
            -v ${variable} \
            ${makelevs} \
            --previous-file ${prevfile}
            >& $(basename ${outfile}.log)
        else
          ${wxajcmd} \
            --from-file filelist.txt \
            -o ${outfile} \
            -v ${variable} \
            ${makelevs} \
            >& $(basename ${outfile}.log)
        fi
      else
        ${wxajcmd} \
          --from-file filelist.txt \
          -o ${outfile} \
          -v ${variable} \
          ${makelevs} \
          >& $(basename ${outfile}.log)
      fi
      ##TimeSteps_checking filelist.txt ${outfile} 'time'

# All frequency variables, but values are statistics for a given period. Adding cell methods too
#
      varstat=`check_stats_varn ${CFvar}`
      if test ! $varstat = '0'; then
        fileoutinf=`get_Ntimes_namelist ${NAMELISToutput} ${DOMAINsim} ${outkind}`
        freqouth2=`echo ${fileoutinf} | awk '{print $1*60/2}'`
        cdo shifttime,-${freqouth2}'second' ${outfile} tmpA.nc
      else
        mv ${outfile} tmpA.nc
      fi

# It is assumed that CDOs give time values at the end of the period (time has to be centered within the interval of the statistics period)
#
      ${ROOTpost}/cellmethods.bash tmpA.nc ${CFvar} ${outkind} All ${NAMELISToutput} ${DOMAINsim} ${RefDate} ${WRF4Ghome} #${syearmon:0:4}-${eyearmon:0:4}

      idate=`date -u +%Y-%m-%d"T00:00:00" -d"${yearmon}01"`
      fdate=`date -u +%Y%m%d -d"${yearmon}01 1 month"`
      fdate=`date -u +%Y-%m-%dT%H:%M:%S -d"${fdate} 1 second ago"`
      cdo seldate,${idate},${fdate} tmpA.nc ${outfile}

# Filling January files with missing values when the calendar is 'noleap'
##
      if test ${yearmon:4:2} = '02'; then
        if test ${Calendar} = 'noleap' && $(isLeap ${yearmon:0:4}); then
          echo "Filling noleap file '"${ofile}"' with missing value matrices for February"
          timedt=`python ${WRF4Ghome}/util/python/nc_var.py -f ${outfile} -o inftime -v time | grep '@dt@' | awk '{print $3*3600}'`
          Nvals=`expr 24 '*' 3600 / ${timedt}`
          python ${WRF4Ghome}/util/python/nc_var.py -f ${outfile} -o valsadd -S 0:last:${Nvals}:missing -v ${CFvar}
        fi
      fi

      mv filelist.txt filelist_${yearmon}.txt
   fi # end between syearmon eyearmon
  done
fi
