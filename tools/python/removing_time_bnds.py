import numpy as np
from optparse import OptionParser
import os
from netCDF4 import Dataset as NetCDFFile
import nc_var_tools as ncvar
import datetime as dt

## g.e. # removing_time_bnds.py -f CCRC_NARCliM_03H_2000-2004_hfss.nc -v hfss -w /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/postprocess/wrfncxnj/wrfncxnj.table
## g.e. # removing_time_bnds.py -f CCRC_NARCliM_01H_1976-1976_tas.nc -v tas -w /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/postprocess/wrfncxnj/wrfncxnj.table
## g.e. # removing_time_bnds.py -f CCRC_NARCliM_MON_1970-1989_praccmean.nc -v praccmean -w /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/postprocess/wrfncxnj/wrfncxnj.table
## g.e. # removing_time_bnds.py -f CCRC_NARCliM_Sydney_MON_1995-1999_hflsmean.nc -v hflsmean -w /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/postprocess/wrfncxnj/wrfncxnj.table

errormsg = 'removing_time_bnds.py: ERROR -- error -- ERROR -- error'
warnmsg = 'removing_time_bnds.py: WARNING -- warning -- WARNING -- warning'

def days_year(year):
    """ Function to give the number of days of a year
    >>> days_year(1976)
    366
    """
    import datetime as dt

    date1=dt.date(year,1,1)
    date2=dt.date(year+1,1,1)

    diffdate = date2-date1
    return diffdate.days

def days_period(yeari,yearf):
    """ Function to give the number of days for a period
    >>> days_period(1976,1980)
    1827
    """

    ndays=0    
    for iyr in range(yearf-yeari+1):
        ndays=ndays+days_year(yeari+iyr)

    return ndays

def printing_class(classobj):
    """ Function to print all the values of a given class
    """

    valscls = vars(classobj)
    for attrcls in valscls:
        print attrcls, ':', valscls[attrcls]

def WRF_CFvariables(tablef):
    """ Provides the conversion of WRF variables name to a CF-convention (CFname, long_name, standard_name, units and postprocesss operation)
      one reading it from a table (wrfncxnj.table)
    """
    import csv
    WRFcfVAR = {}
    for line in csv.reader(open(tablef, "r"), delimiter=" ", skipinitialspace=True):
        if not line[0][0] == '#':
            if len(line[:]) == 6:
                WRFcfVAR[line[0]] = line[1:6]
            else:
                lval = line[1:5]
                lval.append('-')
                WRFcfVAR[line[0]] = lval

    return WRFcfVAR

def is_stats_varn(varN, wrfncxnjtab):
    """Function to check if variable name has a statistic section
    varN: variable name
    wrfncxnjtable: ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table file
    requestedfile: file with the requested variables
    >>> print is_stats_varn('tas', '/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/postprocess/wrfncxnj/wrfncxnj.table', '/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/requested_variables.inf')
    False
    >>> print is_stats_varn('pracc', '/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/postprocess/wrfncxnj/wrfncxnj.table', '/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/requested_variables.inf')
    True
    >>> print is_stats_varn('potevp', '/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/postprocess/wrfncxnj/wrfncxnj.table', '/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/requested_variables.inf')
    True
    """

    statsnames=['min', 'max', 'mean', 'ac', 'mintstep', 'maxtstep', 'meantstep', 'actstep']

    Nstats = len(statsnames)

    for ist in range(Nstats):
        fstat = varN.find(statsnames[ist])
        if fstat != -1:
            return True

# particular cases
##
    specificvars=['pracc']
    for ispec in range(len(specificvars)):
        if specificvars[ispec] == varN:
            return True

# Checking deaccumulation in the 'wrfncxnj.table' file
##
    if not os.path.isfile(wrfncxnjtab):
        print errormsg
        print '  wrfncxnj.table file "' + wrfncxnjtab + '" does not exist !!'
        print errormsg
        quit(-1)    

    WRFnames = WRF_CFvariables(wrfncxnjtab)

    for WRFn in WRFnames:
        if WRFnames[WRFn][0] + '@' == varN + '@':
            if WRFnames[WRFn][4] == 'deaccumulate' or WRFnames[WRFn][4] == 'deaccumulate_flux':
                return True
        
    return False

#######    ########
## MAIN
    #######
parser = OptionParser()

parser.add_option("-f", "--postprocessed_file", dest="pfile", 
                  help="post-processed file to remove time_bnds", metavar="FILENAME")
parser.add_option("-v", "--variable", dest="varname", 
                  help="variable of the file", metavar="VARNAME")
parser.add_option("-w", "--wrfncxnj_table", dest="wfile", 
                  help="wrfncxnj.table file", metavar="FILENAME")

(opts, args) = parser.parse_args()

####### ###### ##### #### ### ## #
varfil = 1.e20

ncvar.filexist(opts.pfile, errormsg, 'post-processed')
ncvar.filexist(opts.wfile, errormsg, 'wrfncxnj.table')

ncpfile = NetCDFFile(opts.pfile,'r')

ncvar.varinfile(ncpfile, opts.pfile, errormsg, '', opts.varname)
varobj = ncpfile.variables[opts.varname]
varinf = ncvar.variable_inf(varobj)

timeinf = ncvar.cls_time_information(ncpfile, 'time')

dimtheor = int((days_period(timeinf.firstTm[0], timeinf.lastTm[0]))*24/timeinf.dt)

print '    removing_time_bnds.py: file time-step: ',timeinf.dt, ' hours'
print '    removing_time_bnds.py: first time step: ', timeinf.firstTS
print '    removing_time_bnds.py: last time step: ', timeinf.lastTS

if not timeinf.dimt == dimtheor:
    print errormsg
    print '  removing_time_bnds.py: File "' + opts.pfile + '" has ',timeinf.dimt, ' time-steps and should have: ',dimtheor
    print '  remove this file to proceed with the others. This one has to be redone!!!'
    print errormsg
    quit(-1)

ncpfile.close()
# Is variable statistics?
##
if is_stats_varn(opts.varname, opts.wfile):
    print 'STATISTIC -- statistic. variable "' + opts.varname + '" is statistic. Checking if variable "time" has the correct values...'
    varfile = ncvar.testvarinfile(ncpfile, opts.pfile, errormsg, '', 'time_bnds')
    if not varfile.exist:
        print varfile.message
        print '  Creating a fake variable "time_bnds". It will be filled propertly later'

        ncpfile = NetCDFFile(opts.pfile,'a')
        ncpfile.createDimension('bnds', 2)
        faketbnds = ncpfile.createVariable('time_bnds', np.float64, ('time', 'bnds',))
        faketbnds[:] = np.zeros((timeinf.dimt, 2), dtype=np.float64)
        newattr = faketbnds.setncattr('calendar', 'standard')
        newattr = faketbnds.setncattr('units', timeinf.units + ' since ' + timeinf.Urefdate)
        ncpfile.sync()
        ncpfile.close()
        ncpfile = NetCDFFile(opts.pfile,'r')
    else:
        ncpfile = NetCDFFile(opts.pfile,'r')

    tbndsobj = ncpfile.variables['time_bnds']
    tbndsinf = ncvar.variable_inf(tbndsobj)

    ncpfile.close()

    if not ncvar.searchInlist(timeinf.attributes, 'bounds'):
        print warnmsg
        print '  file "' + opts.pfile + '" does not have a variable "time" with a "bounds" attribute !!!! adding it!' 
        ncvar.varaddattr('bounds|time_bnds',opts.pfile,'time')

    if not ncvar.searchInlist(tbndsinf.attributes, 'units'):
        print warnmsg
        print '  file "' + opts.pfile + '" does not have a variable "time_bnds" with a "units" attribute !!!! adding it!' 
        ncvar.varaddattr('units|hours!since!1949-12-01!00:00:00',opts.pfile,'time_bnds')
    if not ncvar.searchInlist(tbndsinf.attributes, 'calendar'):
        print warnmsg
        print '  file "' + opts.pfile + '" does not have a variable "time_bnds" with a "calendar" attribute !!!! adding it!' 
        ncvar.varaddattr('calendar|standard',opts.pfile,'time_bnds')

    ncpfile = NetCDFFile(opts.pfile,'r')
    filesecs=opts.pfile.split('_')
    Nfsecs = len(filesecs)
    period=filesecs[Nfsecs-2]
    years=period.split('-')
    yearI=int(years[0])
    yearE=int(years[1])

    nextyearI = yearI + (yearE - yearI + 1)
    nextyearE = yearE + (yearE - yearI + 1)

    prevyearI = yearI - (yearE - yearI + 1)
    prevyearE = yearE - (yearE - yearI + 1)

    print '   period next file: ', nextyearI, '-', nextyearE
    print '   period previous file: ', prevyearI, '-', prevyearE

    timebnds = ncpfile.variables['time_bnds']
    tbndsI = timebnds[0,:]
    tbndsE = timebnds[timeinf.dimt-1,:]
    ncpfile.close()

    tmbndsIim = ncvar.timeref_datetime_mat(timeinf.refdate, tbndsI[0], timeinf.units)
    tmbndsIem = ncvar.timeref_datetime_mat(timeinf.refdate, tbndsI[1], timeinf.units)
    tmbndsEim = ncvar.timeref_datetime_mat(timeinf.refdate, tbndsE[0], timeinf.units)
    tmbndsEem = ncvar.timeref_datetime_mat(timeinf.refdate, tbndsE[1], timeinf.units)
    dtbndsI = tbndsI[1] - tbndsI[0]
    dtbndsE = tbndsE[1] - tbndsE[0]

    if dtbndsI != dtbndsE:
        print errormsg
        print '  I do not know what to do, "time_bnds" are different at the extremes of the period!'
        print '  Initial dtbnds: ',dtbndsI,' hours'
        print '  Final dtbnds: ',dtbndsE,' hours'

    if tmbndsIem[1] - tmbndsIim[1] == 1: 
        print errormsg
        print '  Monthly dt!'
        quit(-1)
    else:
        date0=dt.datetime(yearI, 1, 1, 0, 0, 0) + dt.timedelta(hours=timeinf.dt/2)
        dateN=dt.datetime(yearE+1, 1, 1, 0, 0, 0) - dt.timedelta(hours=timeinf.dt/2)

    if not timeinf.firstTt == date0 or not timeinf.lastTt == dateN:
        print warnmsg
        print '    first time step "', timeinf.firstTt, '" does not match theoretical first step ' + date0.strftime("%Y-%m-%d %H:%M:%S") \
          + ' of period!!!!'
        print '    last time step "', timeinf.lastTt, '" does not match theoretical last step ' + dateN.strftime("%Y-%m-%d %H:%M:%S") \
          + ' of period !!!!'

        if timeinf.firstTt < date0 and timeinf.lastTt < dateN: 
            print '    both times ar smaller than theoretical ones'
            valsum = 'subc,' + str(timeinf.dt/2)
            valauxfile = 'first'
            valchgfile = 'last'
            auxfile = ''
            for isec in range(Nfsecs-1):
                if isec != Nfsecs - 2:
                    auxfile = auxfile + filesecs[isec] + '_'
                else:
                    auxfile = auxfile + str(nextyearI) + '-' + str(nextyearE) + '_'

            print '  displacing ', timeinf.dt/2.,'hours variable "time"'
            auxfile = auxfile + filesecs[Nfsecs-1]

            print '  auxiliar file: ',auxfile
            ncvar.timeshiftvar('1',opts.pfile, opts.varname)
            if not os.path.isfile(auxfile):
                print warnmsg
                print '    auxiliar file "' + auxfile + '" does not exists !!!'
                print '    assuming end of the post-processed period'
                valauxfile = 'zeros'
                ncvar.chgtimestep(valchgfile + ':' + opts.pfile + ':' + valauxfile, opts.pfile, opts.varname)
            else:
                ncvar.chgtimestep(valchgfile + ':' + auxfile + ':' + valauxfile, opts.pfile, opts.varname)

            ncpfile = NetCDFFile(opts.pfile,'a')
            timeobj = ncpfile.variables['time']

            firstdate = date0 - timeinf.refdate
            firstdate = np.float64(firstdate.days * 24 + firstdate.seconds / 3600.)
            timeobj = ncpfile.variables['time']

            timeobj[:] = firstdate + np.float64(range(timeinf.dimt) * timeinf.dt)

            ncpfile.sync()
            ncpfile.close()

        elif timeinf.firstTt > date0 and timeinf.lastTt > dateN: 
            print '    both times ar bigger than theoretical ones'
            valsum = 'subc,' + str(timeinf.dt/2)
            ncvar.valmod(valsum,opts.pfile,'time')

        else:
            print '    I do not know what to do! times have different bigger/smaller as function of the period!'
            print '    time_bnds first time-step :', tmbndsIim, tmbndsIem
            print '    time_bnds last time-step :', tmbndsEim, tmbndsEem

        ncpfile = NetCDFFile(opts.pfile,'a')

        timevals = timeobj[:]

        newtimebnds = np.zeros((timeinf.dimt,2), dtype=np.float64)
        newtimebnds[:,0] = timevals - np.float64(timeinf.dt/2.)
        newtimebnds[:,1] = timevals + np.float64(timeinf.dt/2.)

        tbndsobj = ncpfile.variables['time_bnds']
        tbndsobj[:] = newtimebnds

        ncpfile.sync()
        ncpfile.close()

    else:
        print '  Everything is fine, I do not need to do anything...'
    
else:
    print 'variable "' + opts.varname + '" is not statistic. Checking if variable "time" has the correct values'

    ncpfile = NetCDFFile(opts.pfile,'a')

    filesecs=opts.pfile.split('_')
    Nfsecs = len(filesecs)
    period=filesecs[Nfsecs-2]
    years=period.split('-')
    yearI=int(years[0])
    yearE=int(years[1])

    date0=dt.datetime(yearI, 1, 1, 0, 0, 0)
    dateN=dt.datetime(yearE+1, 1, 1, 0, 0, 0) - dt.timedelta(hours=timeinf.dt)

    if not timeinf.firstTt == date0 or not timeinf.lastTt == dateN:
        print warnmsg
        print '    first time step "', timeinf.firstTt, '" does not match theoretical first step ' + date0.strftime("%Y-%m-%d %H:%M:%S") \
          + ' of period!!!!'
        print '    last time step "', timeinf.lastTt, '" does not match theoretical last step ' + dateN.strftime("%Y-%m-%d %H:%M:%S") \
          + ' of period !!!!'
        firstdate = date0 - timeinf.refdate
        firstdate = np.float64(firstdate.days * 24 + firstdate.seconds / 3600.)
        timeobj = ncpfile.variables['time']

        timeobj[:] = firstdate + np.float64(range(timeinf.dimt) * timeinf.dt)
        ncpfile.sync()
        ncpfile.close()

    else:
        print '  Everything is fine, I do not need to do anything...'

    print 'removing variable "time_bnds"....'
    ncvar.varrm(opts.pfile,'time_bnds')
    ncvar.varrmattr('bounds',opts.pfile,'time')
